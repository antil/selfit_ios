//
//  FindAcademyViewController.swift
//  Selfit
//
//  Created by Priyanka Antil on 07/09/18.
//  Copyright © 2018 valuecoders. All rights reserved.
//

import UIKit

class FindAcademyViewController: SelfitViewController, UITextFieldDelegate {

    
    @IBOutlet weak var txtState: DropdownTextfield!
    @IBOutlet weak var txtUnity: DropdownTextfield!
    @IBOutlet weak var txtCity: DropdownTextfield!
    @IBOutlet weak var txtRegisteredPerson: DropdownTextfield!
    var arrStateNames = [String]()
    var arrCityNames = [String]()
    var states:[States]?
    var cities:[Cities]?
    
    //MARK: - ViewLifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
         setUpNavigationBar(title: localized(key: .Academy))
       
        txtState.delegate = self
        txtCity.delegate = self
        txtUnity.delegate = self
        txtRegisteredPerson.delegate = self

        getStates()
        getCities()
    }
    
    override func viewWillAppear(_ animated: Bool) {
       
        self.navigationController?.isNavigationBarHidden = false
       
        if UserDefaults.standard.value(forKey: "GoToAcademyViewController") as? String == "fromSideMenu" {
        self.setNavigationBarItem()
        }else{
           setUpNavigationBar(title: localized(key: .Academy))
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //MARK: - TextfieldDelegates
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if textField == txtCity{
            textField.resignFirstResponder()
            txtCity.addDropDown(dataArray: arrCityNames)
            txtCity.showDropDown()
        }
        else if textField == txtState{
            textField.resignFirstResponder()
            txtState.addDropDown(dataArray: arrStateNames)
            txtState.showDropDown()
       }
 //           else if textField == txtZipCode{
//            addDoneButtonOnKeyboard(txtZipCode)
//        }
//        else if textField == self.txtDOB{
//
//            self.textfieldDate = self.txtDOB
//            self.pickUpDate(self.textfieldDate)
//
//        }
            
        else{
            
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if textField == txtCity{
            self.view.endEditing(true)
            txtCity.hideDropDown()
        }
        else if textField == txtState{
            self.view.endEditing(true)
            txtState.hideDropDown()
        }else{
            textField.resignFirstResponder()
        }
        return true
    }
    
    
    
    //MARK: - WebServiceMethods
    func getCities(){
        if VTAlmofireHelper.sharedInstance.checkNetworkStatus() == true
        {
            let queryItems = [NSURLQueryItem(name: "codigoEstado", value: "8")]
            let urlComps = NSURLComponents(string: URLManager.manager.getCities)
            urlComps?.queryItems = queryItems as [URLQueryItem]
            let parameterString = urlComps?.query
          ProgressHud.progressBarDisplayer(indicator: true, view: self.view)
            SelfitRequestViewController().getCities(params: parameterString!){ (status, result) -> (Void) in
              ProgressHud.progressBarDisplayer(indicator: false, view: self.view)
                self.cities = status ? result : [Cities]()
                if result.count>0{
                    for i in 0..<result.count{
                        guard let cityName:String = result[i].cityName
                            else{
                                return}
                        self.arrCityNames.append(cityName)
                    }
                    print(self.arrCityNames)
                }
            }
        }
        else
        {
            _ = CustomAlertView().showAlert("", subTitle: localized(key: .NoInternet))
        }
        
    }
    
    func getStates(){
        if VTAlmofireHelper.sharedInstance.checkNetworkStatus() == true
        {
            ProgressHud.progressBarDisplayer(indicator: true, view: self.view)
            SelfitRequestViewController().getStates(){ (status, result) -> (Void) in
             ProgressHud.progressBarDisplayer(indicator: false, view: self.view)
                self.states = status ? result : [States]()
                
                if result.count>0{
                    for i in 0..<result.count{
                        guard let stateName:String = result[i].stateName
                            else{
                                return}
                        self.arrStateNames.append(stateName)
                    }
                    print(self.arrStateNames)
                }
            }
        }
        else
        {
            _ = CustomAlertView().showAlert("", subTitle: localized(key: .NoInternet))
        }
        
    }
    
    func getUnity(){
        
    }
    
    func getRegisteredPersons(){
        
    }
    
}
