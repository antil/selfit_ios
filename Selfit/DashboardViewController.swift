//
//  DashboardViewController.swift
//  Selfit
//
//  Created by Priyanka Antil on 30/08/18.
//  Copyright © 2018 valuecoders. All rights reserved.
//

import UIKit

class DashboardViewController: SelfitViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        setUpNavigationBar(title: localized(key: .Selfit))
        let logo = UIImage(named: "toolbar_logo")
        let imageView = UIImageView(image:logo)
        self.navigationItem.titleView = imageView
        
    }

    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
        self.loadSliderFromAppDelegate()

        self.setNavigationBarItem()
        let rightBtn = UIBarButtonItem(image: UIImage(named: "notification"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(clickNotification))
        self.navigationItem.rightBarButtonItem = rightBtn
        navigationItem.rightBarButtonItem?.tintColor = UIColor.white
    }
            
    @objc func clickNotification(){
        if let notificationsVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "NotificationsViewController") as? NotificationsViewController {
            if let navigator = navigationController {
                navigator.pushViewController(notificationsVC, animated: true)
            }
        }}
        
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
  
    
    //MARK: - IBActions
    @IBAction func actionTraining(_ sender: Any) {
        if let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "MyTrainingViewController") as? MyTrainingViewController {
            if let navigator = navigationController {
                navigator.pushViewController(viewController, animated: true)
            }
        }
    }
    
    @IBAction func actionAcademy(_ sender: Any) {
        if let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "FindAcademyViewController") as? FindAcademyViewController {
             UserDefaults.standard.set("fromDashboard", forKey: "GoToAcademyViewController")
             UserDefaults.standard.synchronize()
            
            if let navigator = navigationController {
                navigator.pushViewController(viewController, animated: true)
            }
        }
    }
 
    
    @IBAction func actionPlan(_ sender: Any) {
        if let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PlanOptionViewController") as? PlanOptionViewController {
            if let navigator = navigationController {
                navigator.pushViewController(viewController, animated: true)
            }
        }
    }
    @IBAction func actionAgenda(_ sender: Any) {
        if let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "MyTrainingViewController") as? MyTrainingViewController {
            if let navigator = navigationController {
                navigator.pushViewController(viewController, animated: true)
            }
        }
    }
    
}
