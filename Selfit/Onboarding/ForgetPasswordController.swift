//
//  ForgetPasswordController.swift
//  Selfit
//
//  Created by Priyanka Antil on 23/08/18.
//  Copyright © 2018 valuecoders. All rights reserved.
//

import UIKit

class ForgetPasswordController: SelfitViewController, UITextFieldDelegate {

    @IBOutlet weak var txtEmail: CustomTextField!
    @IBOutlet weak var txtMyGym: DropdownTextfield!
    @IBOutlet weak var submitBtn: CustomButton!
    
    var arrCompanyNames = [String]()
    var arrCompanyCodes = [String]()
    var companies:[NetworkCompanies]?
    
    //MARK: - ViewLifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
    
         setUpNavigationBar(title: localized(key: .RecoverPassword))
        
        txtMyGym.delegate = self
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(false)
         self.navigationController?.isNavigationBarHidden = false
             submitBtn.backgroundColor = UIColor.clear
        consultNetworkCompanies()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - TextfieldDelegates
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if textField == txtMyGym{
            textField.resignFirstResponder()
            txtMyGym.addDropDown(dataArray: arrCompanyNames)
            txtMyGym.showDropDown()
        }
           
        else{
            
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if textField == txtMyGym{
            self.view.endEditing(true)
            txtMyGym.hideDropDown()
        }
        else{
            textField.resignFirstResponder()
        }
        return true
    }
    
    
    //MARK: ACTIONS
    @IBAction func submitAction(_ sender: UIButton) {
        
        
        if txtEmail.isEmpty() || txtMyGym.isEmpty(){
             _ = CustomAlertView().showAlert("", subTitle: localized(key: .EmptyFieldText))
        }else{
            sender.backgroundColor = UIColor.red
            view.endEditing(true)
            recoverPassword()
        
        }
    }
    
    @IBAction func signBack(_ sender: Any) {
    }
    
    
    //MARK: - WebServices
    func recoverPassword()
    {
        if VTAlmofireHelper.sharedInstance.checkNetworkStatus() == true
        {
           ProgressHud.progressBarDisplayer(indicator: true, view: self.view)
            SelfitRequestViewController().recoverPassword(params: setParametrs()){ (status, response) -> (Void) in
               ProgressHud.progressBarDisplayer(indicator: false, view: self.view)
                if let vc = self.storyboard?.instantiateViewController(withIdentifier: "OTPViewController") as? OTPViewController{
                    vc.emailId = self.txtEmail.text
                    self.navigationController?.pushViewController(vc, animated: true)
                }

            }
        }
        else
        {
             _ = CustomAlertView().showAlert("", subTitle: localized(key: .NoInternet))
        }
        
    }
    
    func setParametrs() -> String{
        let queryItems = [NSURLQueryItem(name: "email", value: txtEmail.text), NSURLQueryItem(name: "empresa", value: "1")]
        let urlComps = NSURLComponents(string: URLManager.manager.recoverPassword)
        urlComps?.queryItems = queryItems as [URLQueryItem]
        let parameterString = urlComps?.query
        print(parameterString!)
        return parameterString!
    }
    
    
    func consultNetworkCompanies(){
        if VTAlmofireHelper.sharedInstance.checkNetworkStatus() == true
        {
            let queryItems = [NSURLQueryItem(name: "chaveRede", value: "chredeselfit")]
            let urlComps = NSURLComponents(string: URLManager.manager.consultNetworsCompanies)
            urlComps?.queryItems = queryItems as [URLQueryItem]
            let parameterString = urlComps?.query
           ProgressHud.progressBarDisplayer(indicator: true, view: self.view)
            
            SelfitRequestViewController().consultNetworkCompanies(params: parameterString!){ (status, result) -> (Void) in
              ProgressHud.progressBarDisplayer(indicator: false, view: self.view)
                self.companies = status ? result : [NetworkCompanies]()
                if result.count>0{
                    for i in 0..<result.count{
                        guard let companyName:String = result[i].companyName
                            else{
                                return}
                        self.arrCompanyNames.append(companyName)
                        guard let companyCode:String = result[i].companyCode
                            else{
                                return}
                        self.arrCompanyCodes.append(companyName)
                        self.arrCompanyNames.append(companyCode)
                    }
                    print(self.arrCompanyNames)
                   print(self.arrCompanyCodes)
                }
            }
            
        }
        else
        {
            _ = CustomAlertView().showAlert("", subTitle: localized(key: .NoInternet))
        }
        
    }
    
    
}
