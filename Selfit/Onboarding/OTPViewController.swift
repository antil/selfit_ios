//
//  OTPViewController.swift
//  Selfit
//
//  Created by Priyanka Antil on 23/08/18.
//  Copyright © 2018 valuecoders. All rights reserved.
//

import UIKit

class OTPViewController: SelfitViewController{

    
    @IBOutlet var pinTextFields: [PinTextfield]!
    var emailId: String!
    override func viewDidLoad() {
        super.viewDidLoad()

        setUpNavigationBar(title: localized(key: .RecoverPassword))
        
        pinTextFields[0].delegate = self
        pinTextFields[1].delegate = self
        pinTextFields[2].delegate = self
        pinTextFields[3].delegate = self
        pinTextFields[4].delegate = self
        pinTextFields[0].becomeFirstResponder()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
     func commitInput() -> Bool {
        
        if (self.pinTextFields[0].text?.isEmpty)!{
            self.pinTextFields[0].becomeFirstResponder()
            return false
        } else if (self.pinTextFields[1].text?.isEmpty)! {
            self.pinTextFields[1].becomeFirstResponder()
            return false
        } else if (self.pinTextFields[2].text?.isEmpty)! {
            self.pinTextFields[2].becomeFirstResponder()
            return false
        }else if (self.pinTextFields[3].text?.isEmpty)! {
            self.pinTextFields[3].becomeFirstResponder()
            return false
        }else if (self.pinTextFields[4].text?.isEmpty)! {
              self.pinTextFields[4].becomeFirstResponder()
             return false
        }else {
            self.verifyOTP()
            return true
        }
        
    }
    
  func verifyOTP() {
    
    self.pinTextFields[4].addDoneButtonOnKeyboard()
    if let vc = self.storyboard?.instantiateViewController(withIdentifier: "ResetPasswordController") as? ResetPasswordController{
        vc.emailId = self.emailId
        self.navigationController?.pushViewController(vc, animated: true)
    }
    }

    
    //MARK: ACTIONS
    @IBAction func resendCodeAction(_ sender: Any) {
    }

}

extension OTPViewController: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        guard string.count > 0 else {
            if let index = self.pinTextFields.index(of: textField as! PinTextfield) {
                if index == 0 {
                    return true
                } else {
                    textField.text = ""
                    self.pinTextFields[index - 1].becomeFirstResponder()
                    return false
                }
            }
            
            return true
        }
        
        let currentText = textField.text ?? ""
        let prospectiveText = (currentText as NSString).replacingCharacters(in: range, with: string)
        
        let newLength = prospectiveText.count
        
        if newLength == 1 {
            if let index = self.pinTextFields.index(of: textField as! PinTextfield) {
                if index == self.pinTextFields.count - 1 {
                    if index == 4 {
                        self.verifyOTP()
                        self.pinTextFields[index].text = string
                        self.pinTextFields[index].becomeFirstResponder()
                        
                    }
                    return false
                } else {
                    textField.text = prospectiveText
                    if let index = self.pinTextFields.index(of: textField as! PinTextfield) {
                        if index == 5 {
                            //    self.checkPasscode()
                            self.pinTextFields[4].addDoneButtonOnKeyboard()
                            self.verifyOTP()
                        }
                    }
                    self.pinTextFields[index+1].becomeFirstResponder()
                    return false
                }
            }
        }
        else if newLength == 0 {
            return true
        } else if newLength > 1 {
            if let index = self.pinTextFields.index(of: textField as! PinTextfield) {
                if index == self.pinTextFields.count - 1 {
                } else {
                    
                    self.pinTextFields[index+1].text = string
                    self.pinTextFields[index+1].becomeFirstResponder()
                    //       self.checkPasscode()
                    return false
                }
            }
            return false
        }
        
        return false
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return commitInput()
    }
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if let index = self.pinTextFields.index(of: textField as! PinTextfield) {
            if index > 0 {
                if (self.pinTextFields[index-1].isEmpty() ){
                    if !self.pinTextFields[index].isEmpty() {
                        return true
                    }
                    return false
                }
            }
        }
        return true
    }
}

