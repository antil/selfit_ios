//
//  RegisterViewController.swift
//  Selfit
//
//  Created by Priyanka Antil on 28/08/18.
//  Copyright © 2018 valuecoders. All rights reserved.
//

import UIKit

class RegisterViewController: SelfitViewController {
    
    
    @IBOutlet weak var txtEmail: CustomTextField!
    @IBOutlet weak var txtPassword: CustomTextField!
    @IBOutlet weak var loginBtn: CustomButton!
    
    var arrGymPlans:[GymPlans]?
    var arrStates:[States]?
    var arrCities:[Cities]?
    
    //MARK: - ViewLifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpNavigationBar(title: localized(key: .Initiatesession))
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        getStates()
        getCities()
        getGymPlanes()
        loginBtn.backgroundColor = UIColor.clear
        self.navigationController?.isNavigationBarHidden = false
    }
   
    
    //MARK:- IBActions
    
    @IBAction func loginAction(_ sender: UIButton) {
        
        if txtEmail.isEmpty() || txtPassword.isEmpty(){
             _ = CustomAlertView().showAlert("", subTitle: localized(key: .EmptyFieldText))
        }
        else if txtEmail.isValidEmail(testStr: txtEmail.text!) == false{
          _ =  CustomAlertView().showAlert("Invalid Email", subTitle: "The email you entered does not exist.Try a valid", closeButtonTitle: "Try Again")
        }
        else{
            sender.backgroundColor = UIColor.red
            self.registerUser()
        }
        self.view.endEditing(true)
    }
    
    @IBAction func rememberMeAction(_ sender: UISwitch) {
        if sender.isSelected == false
        {
            sender.tintColor = UIColor.red
            sender.layer.borderColor = UIColor.clear.cgColor
            
            sender.isSelected = true
            UserDefaults.standard.set(self.txtEmail.text, forKey: "useremailID")
            UserDefaults.standard.set(self.txtPassword.text, forKey: "userpassword")
        }else{
            sender.tintColor = UIColor.clear
            sender.isSelected = false
            UserDefaults.standard.removeObject(forKey: "useremailID")
            UserDefaults.standard.removeObject(forKey: "userpassword")
        }
    }
    
    func registerUser()
    {
        if VTAlmofireHelper.sharedInstance.checkNetworkStatus() == true
        {
            //Utility.main.showHud(message: "Registration in process...")
            SelfitRequestViewController().register(params: setRegisterParametrs()){ (status, response) -> (Void) in
                //Utility.main.hideHud()
                
            }
        }
        else
        {
             _ = CustomAlertView().showAlert("", subTitle: localized(key: .NoInternet))
        }
        
    }
    
    func setRegisterParametrs() -> String{
        
        let queryItems = [NSURLQueryItem(name: "nome", value: "cliente pacto"), NSURLQueryItem(name: "cpf", value: "030.241.370-76"), NSURLQueryItem(name: "email", value: "clientepacto@pactosolucoes.com.br") ,NSURLQueryItem(name: "sexo", value: "M"),NSURLQueryItem(name: "dataNascimento", value: "06/03/1980"),NSURLQueryItem(name: "endereco", value: "rua corumbe"),NSURLQueryItem(name: "complemento", value: "apartment"),NSURLQueryItem(name: "numero", value: "10"),NSURLQueryItem(name: "bairro", value: "Barra de Tijuca"),NSURLQueryItem(name: "cep", value: "74.464-020"),NSURLQueryItem(name: "telCelular", value: "62-12454545"),NSURLQueryItem(name: "telResidencial", value: "62-12454545"),NSURLQueryItem(name: "senha", value: "se123"), NSURLQueryItem(name: "empresa", value: "1"),NSURLQueryItem(name: "codigoCidade", value: "3"),NSURLQueryItem(name: "codigoEstado", value: "2"),NSURLQueryItem(name: "idEmpresaFinanceiroRede", value: "1290")]
        let urlComps = NSURLComponents(string: URLManager.manager.register)
        urlComps?.queryItems = queryItems as [URLQueryItem]
        let parameterString = urlComps?.query
        print(parameterString!)
        return parameterString!
    }
    
    
    
    func getCities(){
//        if VTAlmofireHelper.sharedInstance.checkNetworkStatus() == true
//        {
//            let queryItems = [NSURLQueryItem(name: "codigoEstado", value: "8")]
//            let urlComps = NSURLComponents(string: URLManager.manager.getCities)
//            urlComps?.queryItems = queryItems as [URLQueryItem]
//            let parameterString = urlComps?.query
//            //showloader
//            SelfitRequestViewController().getCities(params: parameterString!){ (status, result) -> (Void) in
//                //hideloader
//                self.arrCities = status ? result : [Cities]()
//                print(self.arrCities![0].cityNome ?? "")
//            }
//        }
//        else
//        {
//            _ = CustomAlertView().showAlert("", subTitle: "No Internet Connection")
//        }
        
    }
    
    func getStates(){
//        if VTAlmofireHelper.sharedInstance.checkNetworkStatus() == true
//        {
//            //Utility.main.showHud(message: "Registration in process...")
//            SelfitRequestViewController().getStates(){ (status, result) -> (Void) in
//                //Utility.main.hideHud()
//                self.arrStates = status ? result : [States]()
//                print(self.arrStates![0].nome ?? "")
//            }
//        }
//        else
//        {
//            _ = CustomAlertView().showAlert("", subTitle: "No Internet Connection")
//        }
        
    }
    
    func getGymPlanes(){
        if VTAlmofireHelper.sharedInstance.checkNetworkStatus() == true
        {
            let queryItems = [NSURLQueryItem(name: "empresa", value: "1"), NSURLQueryItem(name: "codigoPlano", value: "0")]
            let urlComps = NSURLComponents(string: URLManager.manager.gymPlans)
            urlComps?.queryItems = queryItems as [URLQueryItem]
            let parameterString = urlComps?.query
            //showloader
            SelfitRequestViewController().getGymPlans(params: parameterString!){ (status, result) -> (Void) in
                //hideloader
                self.arrGymPlans = status ? result : [GymPlans]()
               // print(self.arrGymPlans![0].codigoHorario ?? 0)
            }
        }
        else
        {
             _ = CustomAlertView().showAlert("", subTitle: localized(key: .NoInternet))
        }
        
    }
    
    
}
