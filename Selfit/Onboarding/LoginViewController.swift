//
//  LoginViewController.swift
//  Selfit
//
//  Created by Priyanka Antil on 20/08/18.
//  Copyright © 2018 valuecoders. All rights reserved.
//

import UIKit
import GoogleSignIn
import FBSDKCoreKit
import FBSDKLoginKit

class LoginViewController: SelfitViewController, GIDSignInUIDelegate, GIDSignInDelegate{

    
    @IBOutlet weak var txtEmail: CustomTextField!
    @IBOutlet weak var txtPassword: CustomTextField!
    
    @IBOutlet weak var loginBtn: CustomButton!
    //MARK: - ViewLifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    
     override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
        loginBtn.backgroundColor = UIColor.clear

    }
    
    
    
    //MARK:- IBActions
    @IBAction func btnRegister(_ sender: Any) {
        
    }
    
    @IBAction func loginAction(_ sender: UIButton) {
//        let vc = self.storyboard?.instantiateViewController(withIdentifier: "DashboardViewController") as? DashboardViewController
//        self.navigationController?.pushViewController(vc!, animated: true)
        if txtEmail.isEmpty() || txtPassword.isEmpty(){
             _ = CustomAlertView().showAlert("", subTitle: localized(key: .EmptyFieldText))
        }
        else if txtEmail.isValidEmail(testStr: txtEmail.text!) == false{
            let alert = CustomAlertView()
             alert.showAlert(localized(key: .InvalidEmail), subTitle: localized(key: .InvalidEmailMessage), closeButtonTitle: localized(key: .TryAgain))
        }
        else{
            sender.backgroundColor = UIColor.red
             login()
        }
        self.view.endEditing(true)
    }
    
   
    
    //MARK: - SocialLogin Methods
    @IBAction func fbLoginAction(_ sender: Any){
       // ProgressHud.progressBarDisplayer(indicator: true, view: self.view)
        
        let fbLoginManager : FBSDKLoginManager = FBSDKLoginManager()
        
        fbLoginManager.loginBehavior = FBSDKLoginBehavior.systemAccount
        //"email","public_profile","user_friends"
        fbLoginManager.logIn(withReadPermissions: ["email","public_profile"], from: self, handler: {(result, error) -> Void in
            if error != nil {
                print("Logged in through facebook" )
                
                print("Facebook Login Error----\n")
                ProgressHud.progressBarDisplayer(indicator: false, view: self.view)
                
            }else if (result?.isCancelled)!{
                print("Facebook Login Canceled----\n")
                ProgressHud.progressBarDisplayer(indicator: false, view: self.view)
                
                
            }
            else {
                self.returnUserData()
                
            }
        }
        )
        
        
    }
    
    
    func returnUserData()
    {
        let userRequest : FBSDKGraphRequest = FBSDKGraphRequest(graphPath: "/me", parameters: ["fields":"id,interested_in,gender,birthday,email,age_range,name,picture.width(480).height(480)"])
        userRequest.start(completionHandler: { (connection, result , error) -> Void in
            
            if ((error) != nil)
            {
                
            }else
            {
                print(result ?? 0.0)
                var dictResult = NSDictionary()
                dictResult = result as! NSDictionary
                
//                if let email: String = dictResult.value(forKey: "email") as? String{
//                    self.userEmail = email
//                }
//                else
//                {
//                    AlertControl.alert(appmassage: "It seems your social id doesn't have an email id.Please try signing up with other social id or register new", view: self)
//                    return
//                }
//                if let phone: NSNumber = dictResult.value(forKey: "phone") as? NSNumber
//                {
//                    self.userPhone = phone
//                }
//
//                self.userName = dictResult.value(forKey: "name") as! String
//                self.socialID = dictResult.value(forKey: "id") as! String
//                self.socialLoginType = "2"
                
//                if let picture:NSDictionary = dictResult.value(forKey: "picture") as? NSDictionary
//                {
//                    let dataPic = picture.value(forKey: "data") as! NSDictionary
//                    self.profilePic = dataPic.value(forKey: "url") as! NSString
//                }
//
//                self.socialLogin()
                
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "DashboardViewController") as? DashboardViewController
                self.navigationController?.pushViewController(vc!, animated: true)
                
            }
        })
        ProgressHud.progressBarDisplayer(indicator: false, view: self.view)
    }
    
    @IBAction func googleLoginAction(_ sender: Any)
    {
        GIDSignIn.sharedInstance().uiDelegate = self
        GIDSignIn.sharedInstance().delegate = self
        GIDSignIn.sharedInstance().signIn()
    }
    
    func sign(inWillDispatch signIn: GIDSignIn!, error: Error!) {
        // myActivityIndicator.stopAnimating()
    }
    
    // Present a view that prompts the user to sign in with Google
    func sign(_ signIn: GIDSignIn!,
              present viewController: UIViewController!) {
        
        self.present(viewController, animated: true, completion: nil)
    }
    
    // Dismiss the "Sign in with Google" view
    func sign(_ signIn: GIDSignIn!,
              dismiss viewController: UIViewController!) {
        self.dismiss(animated: true, completion: nil)
    }
    
    //completed sign In
    public func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        
        if (error == nil) {
            // Perform any operations on signed in user here.
            let userId = user.userID                  // For client-side use only!
            let idToken = user.authentication.idToken // Safe to send to the server
            let fullName = user.profile.name
            let givenName = user.profile.givenName
            let familyName = user.profile.familyName
            let email = user.profile.email
            
//            self.socialID = userId!
//            self.userName = fullName!
//            self.userEmail = email!
//            self.socialLoginType = "1"
            
            
            print(userId ?? 00)
            print(idToken ?? 00)
            print(fullName ?? 00)
            print(givenName ?? 00)
            print(familyName ?? 00)
            print(email ?? 00)
            
            //self.socialLogin()
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "DashboardViewController") as? DashboardViewController
            self.navigationController?.pushViewController(vc!, animated: true)
            
        } else {
            print("\(error.localizedDescription)")
        }
    }
    
    
    //MARK: - WebServices
    func login()
    {
             self.view.endEditing(true)
        if VTAlmofireHelper.sharedInstance.checkNetworkStatus() == true
        {
            ProgressHud.progressBarDisplayer(indicator: true, view: self.view)
            SelfitRequestViewController().login(params: setLoginParametrs()) { (status, response) -> (Void) in
               
                print(response)
                 ProgressHud.progressBarDisplayer(indicator: false, view: self.view)
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "DashboardViewController") as? DashboardViewController
                self.navigationController?.pushViewController(vc!, animated: true)
                
        
                }
            
        }
        else
        {
             _ = CustomAlertView().showAlert("", subTitle: localized(key: .NoInternet))
        }
        
    }
    
    func setLoginParametrs() -> String{
        
        let queryItems = [NSURLQueryItem(name: "email", value: txtEmail.text), NSURLQueryItem(name: "senha", value: txtPassword.text)]
        let urlComps = NSURLComponents(string: URLManager.manager.login)
        urlComps?.queryItems = queryItems as [URLQueryItem]
        let parameterString = urlComps?.query
        print(parameterString!)
        return parameterString!
    }
    
    
}
