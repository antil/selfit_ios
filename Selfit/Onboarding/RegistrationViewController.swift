//
//  RegistrationViewController.swift
//  Selfit
//
//  Created by vinove on 9/7/18.
//  Copyright © 2018 valuecoders. All rights reserved.
//

import UIKit

class RegistrationViewController: SelfitViewController,UITextFieldDelegate,UIPickerViewDelegate {

    @IBOutlet weak var btnChangeData: UIButton!
    @IBOutlet weak var txtCity: DropdownTextfield!
    @IBOutlet weak var txtState: DropdownTextfield!
    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtCellPhone: UITextField!
    @IBOutlet weak var txtTelephone: UITextField!
    @IBOutlet weak var txtDOB: UITextField!
    @IBOutlet weak var txtZipCode: UITextField!
    @IBOutlet weak var txtAddress: UITextField!
    @IBOutlet weak var txtNumber: UITextField!
    @IBOutlet weak var txtNeighbourhood: UITextField!
    @IBOutlet weak var txtComplement: UITextField!
    @IBOutlet weak var changeDataBtn: CustomButton!
    
    @IBOutlet weak var btnSelectFemale: UIButton!
    @IBOutlet weak var btnSelectMale: UIButton!
    var arrGymPlans:[GymPlans]?
    var arrStateNames = [String]()
    var arrCityNames = [String]()
    var states:[States]?
    var cities:[Cities]?
    var countries:[GymPlans]?
    var countryCode:String?
    var datePicker = UIDatePicker()
    var textfieldDate = UITextField()
    var selectedSex:String!
    
    //MARK: ViewLifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()

        //txtState.addImageToRight(txtfield: txtState)
        //txtCity.addImageToRight(txtfield: txtCity)
        
        txtState.delegate = self
        txtCity.delegate = self
        txtDOB.delegate = self
        txtZipCode.delegate = self
        
        self.countryCode = "1"
        setUpNavigationBar(title: localized(key: .Registeration))

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        getStates()
        getCities()
        getGymPlanes()
        consultNetworkCompanies()
        changeDataBtn.backgroundColor = UIColor.clear
        self.navigationController?.isNavigationBarHidden = false
    }
    
   
    
    //MARK:- IBActions
    
    @IBAction func changeDataAction(_ sender: UIButton) {
        
        if txtName.text?.count == 0 || txtNeighbourhood.text?.count == 0 || txtAddress.text?.count == 0 || txtCity.isEmpty() || txtState.text?.count == 0 || txtNumber.text?.count == 0  {
             _ = CustomAlertView().showAlert("", subTitle: localized(key: .EmptyFieldText))
        }
        else if isValidEmail(testStr: txtEmail.text!) == false{
            let alert = CustomAlertView()
            alert.showAlert(localized(key: .InvalidEmail), subTitle: localized(key: .InvalidEmailMessage), closeButtonTitle: localized(key: .TryAgain))
        }
        else{
            sender.backgroundColor = UIColor.red
            self.registerUser()
        }

        self.registerUser()
        self.view.endEditing(true)
    }
    
    
    @IBAction func actionSelectMale(_ sender: UIButton) {
        if sender.isSelected == true{
            sender.isSelected = false
        }else{
           sender.isSelected = true
            selectedSex = "M"
             btnSelectFemale.isSelected = false
        }
    }
    
    @IBAction func actionSelectFemale(_ sender: UIButton) {
        if sender.isSelected == true{
           sender.isSelected = false
        }else{
            sender.isSelected = true
            selectedSex = "F"
            btnSelectMale.isSelected = false
        }
    }
    
   
    //MARK: - WebServices
    func registerUser()
    {
         self.view.endEditing(true)
        if VTAlmofireHelper.sharedInstance.checkNetworkStatus() == true
        {
            ProgressHud.progressBarDisplayer(indicator: true, view: self.view)
            SelfitRequestViewController().register(params: registerParametrs()){ (status, response) -> (Void) in
                ProgressHud.progressBarDisplayer(indicator: false, view: self.view)
                guard let message:String = response["return"] as? String
                    else { return }
                print(message)
                _ = CustomAlertView().showAlert("", subTitle: message)
            }
        }
        else
        {
             _ = CustomAlertView().showAlert("", subTitle: localized(key: .NoInternet))
        }
        
    }
    
    func registerParametrs() -> String{

         let queryItems = [NSURLQueryItem(name: "nome", value: txtName.text), NSURLQueryItem(name: "cpf", value: "65222759458"), NSURLQueryItem(name: "email", value: txtEmail.text) ,NSURLQueryItem(name: "sexo", value: selectedSex),NSURLQueryItem(name: "dataNascimento", value: txtDOB.text),NSURLQueryItem(name: "endereco", value: txtAddress.text),NSURLQueryItem(name: "complemento", value: txtComplement.text),NSURLQueryItem(name: "numero", value: txtNumber.text),NSURLQueryItem(name: "bairro", value: txtNeighbourhood.text),NSURLQueryItem(name: "cep", value: txtZipCode.text),NSURLQueryItem(name: "telCelular", value: txtCellPhone.text),NSURLQueryItem(name: "telResidencial", value: txtTelephone.text),NSURLQueryItem(name: "senha", value: "se123"), NSURLQueryItem(name: "empresa", value: self.countryCode),NSURLQueryItem(name: "codigoCidade", value: "3"),NSURLQueryItem(name: "codigoEstado", value: "2"),NSURLQueryItem(name: "idEmpresaFinanceiroRede", value: "1290")]
        
        let urlComps = NSURLComponents(string: URLManager.manager.register)
        urlComps?.queryItems = queryItems as [URLQueryItem]
        let parameterString = urlComps?.query
        print(parameterString!)
        return parameterString!
    }
    
    
    
    func getCities(){
        if VTAlmofireHelper.sharedInstance.checkNetworkStatus() == true
        {
            let queryItems = [NSURLQueryItem(name: "codigoEstado", value: "8")]
            let urlComps = NSURLComponents(string: URLManager.manager.getCities)
            urlComps?.queryItems = queryItems as [URLQueryItem]
            let parameterString = urlComps?.query
          ProgressHud.progressBarDisplayer(indicator: true, view: self.view)
            SelfitRequestViewController().getCities(params: parameterString!){ (status, result) -> (Void) in
               ProgressHud.progressBarDisplayer(indicator: false, view: self.view)
                self.cities = status ? result : [Cities]()
                if result.count>0{
                    for i in 0..<result.count{
                        guard let cityName:String = result[i].cityName
                            else{
                                return}
                        self.arrCityNames.append(cityName)
                    }
                    print(self.arrCityNames)
                }
            }
        }
        else
        {
            _ = CustomAlertView().showAlert("", subTitle: localized(key: .NoInternet))
        }
        
    }
    
    func getStates(){
        if VTAlmofireHelper.sharedInstance.checkNetworkStatus() == true
        {
         ProgressHud.progressBarDisplayer(indicator: true, view: self.view)
            SelfitRequestViewController().getStates(){ (status, result) -> (Void) in
               ProgressHud.progressBarDisplayer(indicator: false, view: self.view)
                self.states = status ? result : [States]()
                
                if result.count>0{
                    for i in 0..<result.count{
                        guard let stateName:String = result[i].stateName
                        else{
                    return}
                        self.arrStateNames.append(stateName)
                }
                    print(self.arrStateNames)
                }
            }
        }
        else
        {
             _ = CustomAlertView().showAlert("", subTitle: localized(key: .NoInternet))
        }
        
    }
    
    func getGymPlanes(){
        if VTAlmofireHelper.sharedInstance.checkNetworkStatus() == true
        {
            let queryItems = [NSURLQueryItem(name: "empresa", value: "1"), NSURLQueryItem(name: "codigoPlano", value: "0")]
            let urlComps = NSURLComponents(string: URLManager.manager.gymPlans)
            urlComps?.queryItems = queryItems as [URLQueryItem]
            let parameterString = urlComps?.query
          ProgressHud.progressBarDisplayer(indicator: true, view: self.view)
            SelfitRequestViewController().getGymPlans(params: parameterString!){ (status, result) -> (Void) in
             ProgressHud.progressBarDisplayer(indicator: false, view: self.view)
                self.arrGymPlans = status ? result : [GymPlans]()
                //print(self.arrGymPlans![0].codePlan ?? 0)
            }
        }
        else
        {
            _ = CustomAlertView().showAlert("", subTitle: localized(key: .NoInternet))
        }
        
    }
    
    
    func consultNetworkCompanies(){
        if VTAlmofireHelper.sharedInstance.checkNetworkStatus() == true
        {
            let queryItems = [NSURLQueryItem(name: "chaveRede", value: "chredeselfit")]
            let urlComps = NSURLComponents(string: URLManager.manager.consultNetworsCompanies)
            urlComps?.queryItems = queryItems as [URLQueryItem]
            let parameterString = urlComps?.query
            ProgressHud.progressBarDisplayer(indicator: true, view: self.view)
            SelfitRequestViewController().consultNetworkCompanies(params: parameterString!){ (status, result) -> (Void) in
                   ProgressHud.progressBarDisplayer(indicator: false, view: self.view)
                //set country code value
                print(result)
            }
        }
        else
        {
            _ = CustomAlertView().showAlert("", subTitle: localized(key: .NoInternet))
        }
        
    }
    
  
    //MARK: - TextfieldDelegates
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if textField == txtCity{
            textField.resignFirstResponder()
           txtCity.addDropDown(dataArray: arrCityNames)
            txtCity.showDropDown()
        }
        else if textField == txtState{
            textField.resignFirstResponder()
            txtState.addDropDown(dataArray: arrStateNames)
            txtState.showDropDown()
        }else if textField == txtZipCode {
           addDoneButtonOnKeyboard(txtZipCode)
        }else if textField == txtNumber {
            addDoneButtonOnKeyboard(txtNumber)
        }
        else if textField == self.txtDOB{
            
            self.textfieldDate = self.txtDOB
            self.pickUpDate(self.textfieldDate)
            
        }
        
        else{
            
        }
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
       
        if textField == txtCity{
            self.view.endEditing(true)
            txtCity.hideDropDown()
        }
        else if textField == txtState{
            self.view.endEditing(true)
            txtState.hideDropDown()
        }else{
            textField.resignFirstResponder()
        }
        textField.resignFirstResponder()
        return true
    }
    
    //MARK: - KeyboardCustomization
    func addDoneButtonOnKeyboard(_ txt: UITextField)
    {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x:0, y:0, width: 320, height:50))
        doneToolbar.barStyle = UIBarStyle.blackTranslucent
        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: self, action: #selector(self.doneButtonAction))
        let items = NSMutableArray()
        items.add(flexSpace)
        items.add(done)
        doneToolbar.items = items as? [UIBarButtonItem]
        doneToolbar.sizeToFit()
        txt.inputAccessoryView = doneToolbar
    }
    @objc func doneButtonAction()
    {
        txtZipCode.resignFirstResponder()
        
    }
    
    
    
    //MARK: - DatePickerMethods
    func pickUpDate(_ textField : UITextField){
        
        // DatePicker
        self.datePicker = UIDatePicker(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
        self.datePicker.backgroundColor = UIColor.white
        self.datePicker.datePickerMode = UIDatePickerMode.date
        self.datePicker.maximumDate = Date()
        textField.inputView = self.datePicker
        
        // ToolBar
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 92/255, green: 216/255, blue: 255/255, alpha: 1)
        toolBar.sizeToFit()
        
        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(self.doneClick))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(self.cancelClick))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        textField.inputAccessoryView = toolBar
        
    }
    @objc func doneClick() {
        let dateFormatter1 = DateFormatter()
        dateFormatter1.dateFormat = "dd/mm/yyyy"
        self.textfieldDate.text = dateFormatter1.string(from: datePicker.date)
        self.textfieldDate.resignFirstResponder()
    }
    @objc func cancelClick() {
        self.textfieldDate.resignFirstResponder()
    }
    
    //MARK: - EmailValidations
    func isValidEmail(testStr:String) -> Bool
    {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }

}
