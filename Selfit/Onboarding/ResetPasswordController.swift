//
//  ResetPasswordController.swift
//  Selfit
//
//  Created by Priyanka Antil on 20/09/18.
//  Copyright © 2018 valuecoders. All rights reserved.
//

import UIKit

class ResetPasswordController: SelfitViewController {
    
    @IBOutlet weak var txtNewPassword: CustomTextField!
    
    @IBOutlet weak var txtRepeatPassword: CustomTextField!
    var emailId: String!
    
    @IBOutlet weak var updatePasswordBtn: CustomButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        
         setUpNavigationBar(title: localized(key: .RecoverPassword))
      
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
        updatePasswordBtn.backgroundColor = UIColor.clear
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
   
    //MARK: ACTIONS
    @IBAction func updatePasswordAction(_ sender: UIButton) {
        if txtNewPassword.isEmpty() || txtRepeatPassword.isEmpty(){
            _ = CustomAlertView().showAlert("", subTitle: localized(key: .EmptyFieldText))
        }else{
          changePassword()
        }
    }
    
    @IBAction func signBack(_ sender: Any) {
    }
    
    
    //MARK: - WebServices
    func changePassword()
    {
        if VTAlmofireHelper.sharedInstance.checkNetworkStatus() == true
        {
           ProgressHud.progressBarDisplayer(indicator: true, view: self.view)
            SelfitRequestViewController().changePassword(params: setParametrs()){ (status, response) -> (Void) in
               ProgressHud.progressBarDisplayer(indicator: false, view: self.view)
                
                 _ = CustomAlertView().showAlert(self.localized(key: .PasswordChanged), subTitle: self.localized(key: .PasswordChangedMessage))
                
//                let alert = CustomAlertView()
//               alert.showAlert(self.localized(key: .PasswordChanged), subTitle: self.localized(key: .PasswordChangedMessage))
//                alert.appearance.showCloseButton = false
//                alert.addButton(self.localized(key: .OK), action: {
                     self.navigationController?.popToRootViewController(animated: true)
                //})
            }
        }
        else
        {
            _ = CustomAlertView().showAlert("", subTitle: localized(key: .NoInternet))
        }
        
    }
    
    
    
    func setParametrs() -> String{
        
        let queryItems = [NSURLQueryItem(name: "email", value: self.emailId), NSURLQueryItem(name: "senha", value: txtNewPassword.text)]
        let urlComps = NSURLComponents(string: URLManager.manager.changePassword)
        urlComps?.queryItems = queryItems as [URLQueryItem]
        let parameterString = urlComps?.query
        print(parameterString!)
        return parameterString!
}

}
    
