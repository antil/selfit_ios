//
//  SliderMenuCell.swift
//  Selfit
//
//  Created by Priyanka Antil on 03/09/18.
//  Copyright © 2018 valuecoders. All rights reserved.
//

import UIKit

class SliderMenuCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var titleImage: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
