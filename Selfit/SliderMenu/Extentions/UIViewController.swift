//
//  UIViewControllerExtension.swift
//  SlideMenuControllerSwift
//
//  Created by Yuji Hato on 1/19/15.
//  Copyright (c) 2015 Yuji Hato. All rights reserved.
//

import UIKit

extension UIViewController {
  
    func setNavigationBarItem() {
   
        self.navigationItem.rightBarButtonItem = nil
        let textAttributes = [NSAttributedStringKey.foregroundColor:UIColor.white, NSAttributedStringKey.font: UIFont(name: "Ubuntu-Bold", size: 18)!]
        self.navigationController?.navigationBar.titleTextAttributes = textAttributes
         self.navigationController?.navigationBar.barTintColor = UIColor.init(red: 6/255.0, green: 35/255.0, blue: 128/255.0, alpha: 1.0)
        self.addLeftBarButtonWithImage(UIImage.init(named: "menu-button")!)
      
    }
   
    
    func removeNavigationBarItem() {
        self.navigationItem.leftBarButtonItem = nil
        self.navigationItem.rightBarButtonItem = nil
        self.slideMenuController()?.removeLeftGestures()
        self.slideMenuController()?.removeRightGestures()
    }
    func loadSliderFromAppDelegate() {
        let appdel = UIApplication.shared.delegate as! AppDelegate
        if (appdel.window?.rootViewController as? ExSlideMenuController) == nil {
            appdel.loadSlider()
        }
    }
}
