//
//  SliderTableViewController.swift
//  Selfit
//
//  Created by Priyanka Antil on 03/09/18.
//  Copyright © 2018 valuecoders. All rights reserved.
//

import UIKit

enum MenuItems: Int {
    case MAIN = 0
    case MY_PROFILE
    case MY_ACADEMY
    case LESSONS_AND_EVENTS
    case MY_CHALLENGES
    case SETTINGS
    case LOGOUT
}

protocol LeftMenuProtocol : class {
    func navigateToController(_ menu: MenuItems)
}

class SliderTableViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    var mainViewController: UIViewController!
    var settingsViewController: UIViewController!
    var profileViewController: UIViewController!
    var notificationsViewController: UIViewController!
    var academyViewController: UIViewController!
    
    //MARK: - ViewLifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
       
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let settingsVC = storyboard.instantiateViewController(withIdentifier: "SettingsViewController") as! SettingsViewController
        self.settingsViewController = UINavigationController(rootViewController: settingsVC)
        
        let profileVC = storyboard.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
        self.profileViewController = UINavigationController(rootViewController: profileVC)
        
        let academyVC = storyboard.instantiateViewController(withIdentifier: "FindAcademyViewController") as! FindAcademyViewController
        self.academyViewController = UINavigationController(rootViewController: academyVC)
        
    }
    
    
    func navigateToController(_ menu: MenuItems) {
        switch menu {
        case .MAIN:
            self.slideMenuController()?.changeMainViewController(self.mainViewController, close: true)
            break
        case .MY_PROFILE:
            self.slideMenuController()?.changeMainViewController(self.profileViewController, close: true)
            break
        case .MY_ACADEMY:
            UserDefaults.standard.set("fromSideMenu", forKey: "GoToAcademyViewController")
            UserDefaults.standard.synchronize()
             self.slideMenuController()?.changeMainViewController(self.academyViewController, close: true)
            break
            
        case .LESSONS_AND_EVENTS: break
            
        case .MY_CHALLENGES:
            
            break
            
        case .SETTINGS:
            self.slideMenuController()?.changeMainViewController(self.settingsViewController, close: true)
            break
            
        case .LOGOUT:
            let alert = CustomAlertView()
            alert.addButton("Cancel", backgroundColor: UIColor.lightGray ) {
                alert.hideView()
            }
            alert.addButton("Exit", backgroundColor: UIColor.red ) {
                let rootviewcontroller: UIWindow = ((UIApplication.shared.delegate?.window)!)!
                rootviewcontroller.rootViewController = self.storyboard?.instantiateViewController(withIdentifier: "IntroViewController")
            }
            alert.appearance.showCloseButton = false
            alert.showAlert("Exit the App", subTitle: "Are You Sure You Want to Exit the App?")
            
            break
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
extension SliderTableViewController:UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return (MAX_HEIGHT - 200) / 6
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return leftMenuCellTitles.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:SliderMenuCell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath as IndexPath) as! SliderMenuCell
        cell.titleLabel?.text = leftMenuCellTitles[indexPath.row]
        cell.titleLabel?.textColor = UIColor.groupTableViewBackground
        cell.titleImage?.image = UIImage.init(named: leftMenuCellTitleImages[indexPath.row])
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let menu = MenuItems(rawValue: indexPath.row) {
            self.navigateToController(menu)
        }
    }
}
