//
//  SettingsViewController.swift
//  Selfit
//
//  Created by Priyanka Antil on 30/08/18.
//  Copyright © 2018 valuecoders. All rights reserved.
//

import UIKit


class SettingsViewController: SelfitViewController {
    
    var arrSecurityOptions = [String]()
    var arrPrivacyOptions = [String]()
    var arrAppOptions = [String]()
    
    //MARK: - ViewLifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpNavigationBar(title: localized(key: .Settings))
        arrSecurityOptions = [localized(key: .ChangePassword),localized(key: .TouchID)]
        arrPrivacyOptions = [localized(key: .ActivateLocation),localized(key: .ActivateNotification)]
        arrAppOptions = [localized(key: .AboutSelfit),localized(key: .SendFeedBack), localized(key: .ShareApplication), localized(key: .RateOnAppStore), localized(key: .TermsCondition)]
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setNavigationBarItem()
    }
    
    func changeController(cont:NavigationFlow, option:SettingsControllers ) {
        let viewController = mainStoryboard.instantiateViewController(withIdentifier: cont.rawValue)
        if viewController.isKind(of: SettingOptionsViewController.self) {
            let cntr = viewController as! SettingOptionsViewController
            if let navigator = navigationController {
                navigator.pushViewController(cntr, animated: true)
                cntr.rawValue = option.rawValue
            }
        }
        else{
            if let navigator = navigationController {
                navigator.pushViewController(viewController, animated: true)
            }
        }
    }
    
    func shareApplication(){
        let activityViewController = UIActivityViewController(
            activityItems: ["SELFIT", "https://www.google.co.in"], applicationActivities: nil)
        activityViewController.setValue("SELFIT", forKey: "subject")
        activityViewController.popoverPresentationController?.sourceView = self.view
        
        activityViewController.excludedActivityTypes = [
            UIActivityType.saveToCameraRoll,
            UIActivityType.addToReadingList,
            UIActivityType.assignToContact,
            UIActivityType.print,
            UIActivityType.openInIBooks,
            UIActivityType(rawValue: "com.apple.reminders.RemindersEditorExtension"),
            //            UIActivityType(rawValue: "com.apple.mobilenotes.SharingExtension"),
        ]
        self.navigationController?.present(activityViewController, animated: true, completion: nil)
    }
    
    func rateOnAppstore() {
        if let url = URL(string:"https://www.google.co.in") {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
    }
    
    func navigateToController(_ menu: SettingsControllers)  {
        switch menu {
        case .ChangePassword:
            break
        case .ActivateTouchID:
            changeController(cont: .SettingsOptionViewController, option: .ActivateTouchID)
            break
        case .ActivateLocation:
            changeController(cont: .SettingsOptionViewController, option: .ActivateLocation)
            break
        case .ActivateNotification:
            changeController(cont: .SettingsOptionViewController, option: .ActivateNotification)
            break
        case .AboutSelfit:
            changeController(cont: .AboutSelfitController, option: .AboutSelfit)
            break
        case .SendFeedBack:
            changeController(cont: .SendFeedbackController, option: .SendFeedBack)
            break
        case .ShareApplication:
            shareApplication()
            break
        case .RateOnAppstore:
            rateOnAppstore()
            break
        case .TermsAndCondition:
            changeController(cont: .TermsViewController, option: .TermsAndCondition)
            break
        }
    }
    
    
    
}

extension SettingsViewController: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 2{
            return 5
        }
        else{
            return 2
        }
    }
    
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let returnedView = UIView(frame: CGRect(x: 0, y: 0,width: tableView.bounds.size.width, height:50))
        let label = UILabel(frame: CGRect(x: 28, y: 4, width: tableView.bounds.size.width - 56, height: returnedView.frame.size.height - 8))
        switch section {
        case 0: label.text = localized(key: .Security)
        case 1: label.text = localized(key: .Privacy)
        case 2: label.text = localized(key: .App)
        default:
            label.text = localized(key: .App)
        }
        
        label.textColor = UIColor.white
       // returnedView.backgroundColor = UIColor.init(red: 18/255.0, green: 70/255.0, blue: 185/255.0, alpha: 1.0)
        returnedView.backgroundColor = UIColor.clear
        returnedView.addSubview(label)
        return returnedView
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:SettingsTableViewCell = tableView.dequeueReusableCell(withIdentifier: "settingsCell", for: indexPath as IndexPath) as! SettingsTableViewCell

        let activateSwitch = UISwitch(frame: .zero)
        activateSwitch.tintColor = UIColor.init(red: 93/255.0, green: 134/255.0, blue: 202/255.0, alpha: 1.0)
//        activateSwitch.setOn(false, animated: true)
  //      switchView.addTarget(self, action: #selector(self.switchChanged(_:)), for: .valueChanged)
        activateSwitch.onTintColor = UIColor.red
        activateSwitch.backgroundColor = UIColor.clear
        if indexPath.section == 0{
            cell.titleLabel.text = arrSecurityOptions[indexPath.row]
            cell.accessoryType = .none
            if indexPath.row == 0{
                cell.accessoryType = .disclosureIndicator
            }else
            {
                activateSwitch.tag = indexPath.row
                cell.accessoryView = activateSwitch
            }
        }else if indexPath.section == 1{
            cell.titleLabel.text = arrPrivacyOptions[indexPath.row]
            activateSwitch.tag = indexPath.row
            cell.accessoryView = activateSwitch
            
        }else{
            cell.titleLabel.text = arrAppOptions[indexPath.row]
            cell.accessoryType = .disclosureIndicator
        }
        return cell
    }
    
    func switchChanged(_ sender : UISwitch!){
        
        print("table row switch Changed \(sender.tag)")
        print("The switch is \(sender.isOn ? "ON" : "OFF")")
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 0{
            
            if let selected = SettingsControllers(rawValue: indexPath.row) {
                self.navigateToController(selected)
                
            }
        }else if indexPath.section == 1{
            if let selected = SettingsControllers(rawValue: indexPath.row+2) {
                self.navigateToController(selected)
            }
        }else{
            if let selected = SettingsControllers(rawValue: indexPath.row+4) {
                self.navigateToController(selected)
            }
        }
    }
    
    
}

