//
//  NotificationsDetailController.swift
//  Selfit
//
//  Created by Priyanka Antil on 09/10/18.
//  Copyright © 2018 valuecoders. All rights reserved.
//

import UIKit

class NotificationsDetailController: SelfitViewController {

    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    
    //MARK: - ViewLifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()

        setUpNavigationBar(title: localized(key: .Notifications))
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    

}
