//
//  CancelAccountTableViewCell.swift
//  Selfit
//
//  Created by vinove on 9/4/18.
//  Copyright © 2018 valuecoders. All rights reserved.
//

import UIKit

class CancelAccountTableViewCell: UITableViewCell {

    @IBOutlet weak var btnSelection: UIButton!
    @IBOutlet weak var lblReasonName: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
