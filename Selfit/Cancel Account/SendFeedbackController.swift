//
//  SendFeedbackController.swift
//  Selfit
//
//  Created by vinove on 9/5/18.
//  Copyright © 2018 valuecoders. All rights reserved.
//

import UIKit

class SendFeedbackController: SelfitViewController, UITextViewDelegate {

    @IBOutlet weak var txtView: UITextView!
    override func viewDidLoad() {
        super.viewDidLoad()

        self.txtView.delegate = self
        self.txtView.layer.cornerRadius = 7.0
        self.txtView.layer.masksToBounds = true
        self.txtView.layer.borderColor = UIColor.red.cgColor
        self.txtView.layer.borderWidth = 1.0
       setUpNavigationBar(title: localized(key: .SendFeedBack))
        
        }
        
        override func viewWillAppear(_ animated: Bool) {
            self.navigationController?.isNavigationBarHidden = false
        }
        override func didReceiveMemoryWarning() {
            super.didReceiveMemoryWarning()
            // Dispose of any resources that can be recreated.
        }
        
       
       
        
    func textView(textView: UITextView, shouldChangeTextInRange range: NSRange, replacementText text: String) -> Bool {
        if (text == "\n") {
            textView.resignFirstResponder()
        }
        return true
    }
    


}
