//
//  CancelAccountViewController.swift
//  Selfit
//
//  Created by vinove on 9/4/18.
//  Copyright © 2018 valuecoders. All rights reserved.
//

import UIKit

class CancelAccountViewController: SelfitViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var heightTable: NSLayoutConstraint!
    @IBOutlet weak var tableView: UITableView!
     let arrReason = ["Não vou voltar a usar a APP","Tenho outra conta","Demasiadas notificações","A APP não fucniona bem","Outro motivo"]
    
    var indexValue = Int()
    

    override func viewDidLoad() {
        super.viewDidLoad()

        self.heightTable.constant = CGFloat(44 * (arrReason.count + 1) + 50)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrReason.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeueReusableCell(withIdentifier: "CancelAccountTableViewCell") as! CancelAccountTableViewCell
        cell.lblReasonName.text = self.arrReason[indexPath.row]
        
        if self.indexValue == indexPath.row
        {
            cell.btnSelection.setImage(UIImage(named: "whiteTick"), for: .normal)
        }else{
            cell.btnSelection.setImage(UIImage(named: ""), for: .normal)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.indexValue = indexPath.row
        self.tableView.reloadData()
    }
    
   
    
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

}

