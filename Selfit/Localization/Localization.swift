//
//  Localization.swift
//  Selfit
//
//  Created by Priyanka Antil on 14/09/18.
//  Copyright © 2018 valuecoders. All rights reserved.
//

import Foundation
enum TranslationKey:String {
    
    case Cancel = "cancel",
    OK = "ok",
    Done = "done",
    Next = "next",
    Back = "back",
    Yes = "yes",
    No = "no",
    
    
    //SETTINGS MENU
    
    ActivateLocation = "activate_location",
    ActivateTouchID = "activate_touchId",
    ActivateNotification = "activate_notifictaions",
    ChangePassword = "change_password",
    TouchID = "touchId",
    AboutSelfit = "about_selfit",
    SendFeedBack = "send_feedback",
    ShareApplication = "share_application",
    RateOnAppStore = "rate_on_appstore",
    TermsCondition = "terms_and_conditions",
    Security = "security",
    Privacy = "privacy",
    App = "app",
    Accept = "accept",
    NotNow = "not_now",
    LogOut = "logout",
    
    /* Onboarding */
    Login = "login",
    LoginViaFacebook = "login_via_facebook",
    LoginViaGoogle = "login_via_google",
    RegisterHere = "register_here",
    ProfileResendCode = "resend_code",
    NoInternet = "no_internet_connection",
    EmptyFieldText = "empty_field_text",
    InvalidEmail = "invalid_email",
    InvalidEmailMessage = "invalid_email_error_message",
    TryAgain = "try_again",
    PasswordChangedMessage = "password_changed_message",
    PasswordChanged = "password_changed",
    LogoutMessage = "logout_message",
    LogoutTitle = "logout_title",
    
    //NavigationBarTitles
    Selfit = "selfit",
    MyTraining = "my_training",
    Academy = "academy",
    MyPlan = "my_plan",
    PaymentStatus = "payment_status",
    ChangeCreditCard = "change_credit_card",
    PaymentHistory = "history_of_payment",
    About = "about",
    Profile = "profile",
    MyAgenda = "my_agenda",
    RecoverPassword = "recover_password",
    Registeration = "registeration",
    Initiatesession = "initiate_session",
    Settings = "settings",
    Notifications = "notifications",
    FiscalNote = "Fiscal_note",
    
    //Training
    TrainingSheet = "training_sheet",
    Activities = "activities"
    
}
