//
//  TrainingArea.swift
//  Selfit
//
//  Created by Priyanka Antil on 01/10/18.
//  Copyright © 2018 valuecoders. All rights reserved.
//

import UIKit

class TrainingArea: NSObject {
    var fichasName:String!
   
}

class ParseTrainingArea {
    func parseData(params:Array<[String:Any]>, compilition:@escaping (_ status:Bool,_ result:[TrainingArea]) ->(Void))   {
        var dataArray = Array<TrainingArea>()

        for item in params {
            let object = TrainingArea()
            if let fichasName = item["nome"] as? String{
                object.fichasName = fichasName
            }
           
            dataArray.append(object)
        }
        compilition(true,dataArray)
     }
}

