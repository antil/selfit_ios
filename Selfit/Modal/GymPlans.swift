//
//  GymPlansModal.swift
//  Selfit
//
//  Created by Priyanka Antil on 05/09/18.
//  Copyright © 2018 valuecoders. All rights reserved.
//

import UIKit

class GymPlans: NSObject {
    var timeCode:Int?
    var codePlan:Int?
    var descript:String?
    var hoursDescription:String?
    var conditionPayment:String?
    var descricaoEncantamento:String?
    var monthlyFee:Float?
    var registerationFee:Int?
    var accessionFee:Int?
    var annualFee:Float?
    var annualDay:Int?
    var annualMonth:String?
    var planDuration:Int? //number of months of the plan
    var modalities:Array<[String:Any]>? //available plan modality
}

class ParseGymPlans {
    func parseData(params:Array<[String:Any]>, compilition:@escaping (_ status:Bool,_ result:[GymPlans]) ->(Void))   {
        var dataArray = Array<GymPlans>()
        
        for item in params {
            let object = GymPlans()
            if let timeCode = item["codigoHorario"] as? Int{
                object.timeCode = timeCode
            }
            if let codePlan = item["codigoPlano"] as? Int{
                object.codePlan = codePlan
            }
            if let descript = item["descricao"] as? String{
                object.descript = descript
            }
            if let hoursDescription = item["descricaoHorario"] as? String{
                object.hoursDescription = hoursDescription
            }
            if let conditionPayment = item["condicaoPagamento"] as? String{
                object.conditionPayment = conditionPayment
            }
            if let descricaoEncantamento = item["descricaoEncantamento"] as? String{
                object.descricaoEncantamento = descricaoEncantamento
            }
            if let monthlyFee = item["valorMensal"] as? Float{
                object.monthlyFee = monthlyFee
            }
            if let registerationFee = item["valorMatricula"] as? Int{
                object.registerationFee = registerationFee
            }
            if let accessionFee = item["taxaAdesao"] as? Int{
                object.accessionFee = accessionFee
            }
            if let annualFee = item["valorAnuidade"] as? Float{
                object.annualFee = annualFee
            }
            if let annualDay = item["diaAnuidade"] as? Int{
                object.annualDay = annualDay
            }
            if let annualMonth = item["mesAnuidade"] as? String{
                object.annualMonth = annualMonth
            }
            if let planDuration = item["duracaoPlano"] as? Int{
                object.planDuration = planDuration
            }
            if let modalities = item["modalidades"] as? Array<[String:Any]>{
                object.modalities = modalities
            }
            dataArray.append(object)
        }
        compilition(true,dataArray)
        
    }
}
