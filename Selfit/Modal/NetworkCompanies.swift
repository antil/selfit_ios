//
//  NetworkCompanies.swift
//  Selfit
//
//  Created by Priyanka Antil on 01/10/18.
//  Copyright © 2018 valuecoders. All rights reserved.
//

import UIKit

class NetworkCompanies: NSObject {
    var id:Int?
    var companyName:String?
    var key:String?
    var city:String?
    var status:String?
    var address:String?
    var companyCode:String?
    var telephone:String?
    var cnpj:String?
    var allowSite:Bool?
}

class ParseNetworkCompanies {
    func parseData(params:Array<[String:Any]>, compilition:@escaping (_ status:Bool,_ result:[NetworkCompanies]) ->(Void))   {
        var dataArray = Array<NetworkCompanies>()
       
        for item in params {
             let object = NetworkCompanies()
            if let id = item["id"] as? Int{
                object.id = id
            }
            if let companyName = item["nomeEmpresa"] as? String{
                object.companyName = companyName
            }
            if let address = item["endereco"] as? String{
                object.address = address
            }
            if let key = item["chave"] as? String{
                object.key = key
            }
            if let status = item["estado"] as? String{
                object.status = status
            }
            if let companyCode = item["empresaZW"] as? String{
                object.companyCode = companyCode
            }
            if let city = item["cidade"] as? String{
                object.city = city
            }
            if let telephone = item["telefone"] as? String{
                object.telephone = telephone
            }
            if let cnpj = item["cnpj"] as? String{
                object.cnpj = cnpj
            }
            if let allowSite = item["permiteVendaNoSite"] as? Bool{
                object.allowSite = allowSite
            }
            dataArray.append(object)
        }
        compilition(true,dataArray)
        
    }
}

