//
//  Cities.swift
//  Selfit
//
//  Created by Priyanka Antil on 06/09/18.
//  Copyright © 2018 valuecoders. All rights reserved.
//

import UIKit

class Cities: NSObject {

    var cityCode:Int? //city code
    var cityName:String? //city name
    var stateCode:Int? //state code
}

class ParseCities {
    func parseData(params:Array<[String:Any]>, compilition:@escaping (_ status:Bool,_ result:[Cities]) ->(Void))   {
        var dataArray = Array<Cities>()
       
        for item in params {
             let object = Cities()
            if let cityCode = item["codigo"] as? Int{
                object.cityCode = cityCode
            }
            if let cityName = item["nome"] as? String{
                object.cityName = cityName
            }
            if let stateCode = item["codigoEstado"] as? Int{
                object.stateCode = stateCode
            }
            dataArray.append(object)
        }
        compilition(true,dataArray)
        
    }
}
