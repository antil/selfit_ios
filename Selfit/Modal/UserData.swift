//
//  File.swift
//  Selfit
//
//  Created by Priyanka Antil on 19/09/18.
//  Copyright © 2018 valuecoders. All rights reserved.
//


import UIKit

class UserData: NSObject {
    
    var code:Int?
    var personCode:Int?
    var name:String?
    var registerationId:Int?
    var email:String?
    var sex:String?
    var residentialNumber:String?
    var cellularNumber:String?
    var company:Int?
    var posts:String?
    var address:String?
    var birthdate:String?
    var registerationDate:String?
    var codConsultor:String?
    var consultant:String?
    var situation:String?
    var companyName:String?
    var apartment:String?
    var houseNumber:Int?
    var neighborhoodName:String?
    var cep:String?
    var contractCommit:Bool?
    var cityCode:Int?
    var stateCode:Int?
    var financialCompanyId:Int?
    var cpf:String?
    var socialNetworkToken:String?
    var socialNetworkEmail:String?
    var nameUserMobile:String?
    var ZWKey:String?
}

class ParseUserData {
    func parseData(params:Dictionary<String, Any>, compilition:@escaping (_ status:Bool,_ result:UserData) ->(Void))   {
        let item = params
        let object = UserData()
        if let code = item["codigo"] as? Int{
            object.code = code
        }
        if let personCode = item["codigoPessoa"] as? Int{
            object.personCode = personCode
        }
        if let name = item["nome"] as? String{
            object.name = name
        }
        if let registerationId = item["matricula"] as? Int{
            object.registerationId = registerationId
        }
        if let email = item["email"] as? String{
            object.email = email
        }
        if let sex = item["sexo"] as? String{
            object.sex = sex
        }
        if let residentialNumber = item["telResidencial"] as? String{
            object.residentialNumber = residentialNumber
        }
        if let cellularNumber = item["telCelular"] as? String{
            object.cellularNumber = cellularNumber
        }
        if let company = item["empresa"] as? Int{
            object.company = company
        }
        if let posts = item["mensagens"] as? String{
            object.posts = posts
        }
        if let birthdate = item["dataNascimento"] as? String{
            object.birthdate = birthdate
        }
        if let address = item["endereco"] as? String{
            object.address = address
        }
        if let registerationDate = item["dataCadastro"] as? String{
            object.registerationDate = registerationDate
        }
        if let codConsultor = item["codConsultor"] as? String{
            object.codConsultor = codConsultor
        }
        if let consultant = item["consultor"] as? String{
            object.consultant = consultant
        }
        if let situation = item["situacao"] as? String{
            object.situation = situation
        }
        if let companyName = item["nomeEmpresa"] as? String{
            object.companyName = companyName
        }
        if let apartment = item["complemento"] as? String{
            object.apartment = apartment
        }
        if let houseNumber = item["numero"] as? Int{
            object.houseNumber = houseNumber
        }
        if let neighborhoodName = item["bairro"] as? String{
            object.neighborhoodName = neighborhoodName
        }
        if let cep = item["cep"] as? String{
            object.cep = cep
        }
        if let contractCommit = item["permiteContratosConcomitante"] as? Bool{
            object.contractCommit = contractCommit
        }
        if let cityCode = item["codigoCidade"] as? Int{
            object.cityCode = cityCode
        }
    
        if let stateCode = item["codigoEstado"] as? Int{
            object.stateCode = stateCode
        }

        if let financialCompanyId = item["idEmpresaFinanceiroRede"] as? Int{
            object.financialCompanyId = financialCompanyId
        }
        if let nameUserMobile = item["nomeUsuarioMovel"] as? String{
            object.nameUserMobile = nameUserMobile
        }
        if let ZWKey = item["chaveZW"] as? String{
            object.ZWKey = ZWKey
        }
        if let cpf = item["cpf"] as? String{
            object.cpf = cpf
        }
        if let socialNetworkToken = item["tokenRedeSocial"] as? String{
            object.socialNetworkToken = socialNetworkToken
        }
        if let socialNetworkEmail = item["tokenRedeSocial"] as? String{
            object.socialNetworkEmail = socialNetworkEmail
        }
        compilition(true,object)
        
    }
}




