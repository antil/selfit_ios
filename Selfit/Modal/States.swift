//
//  States.swift
//  Selfit
//
//  Created by Priyanka Antil on 06/09/18.
//  Copyright © 2018 valuecoders. All rights reserved.
//

import UIKit

class States: NSObject {
    var stateCode:Int? //state code
    var stateName:String? //state name
    var acronym:String? //state acronym
    var countryCode:Int? //country code
}

class ParseStates {
    func parseData(params:Array<[String:Any]>, compilition:@escaping (_ status:Bool,_ result:[States]) ->(Void))   {
        var dataArray = Array<States>()
        
        for item in params {
            let object = States()
            if let stateCode = item["codigo"] as? Int{
                object.stateCode = stateCode
            }
            if let countryCode = item["codigoPais"] as? Int{
                object.countryCode = countryCode
            }
            if let stateName = item["nome"] as? String{
                object.stateName = stateName
            }
            if let acronym = item["sigla"] as? String{
                object.acronym = acronym
            }
            dataArray.append(object)
        }
        compilition(true,dataArray)
        
    }
}
