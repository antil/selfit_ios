//
//  AgendaViewController.swift
//  Selfit
//
//  Created by Priyanka Antil on 07/09/18.
//  Copyright © 2018 valuecoders. All rights reserved.
//

import UIKit

class AgendaViewController: SelfitViewController,UITableViewDelegate,UITableViewDataSource {
    
    
    @IBOutlet weak var tableView: UITableView!
    let arrData = ["BIKE HORIZONTAL No 35","OBJECTIVO","PROGRAMA","ÍNICIO","FIM","FICHA","FREQUÊNCIA"]
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
         setUpNavigationBar(title: localized(key: .MyAgenda))
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
   
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeueReusableCell(withIdentifier: "MyTrainingTableViewCell") as! MyTrainingTableViewCell
        cell.lblName.text = self.arrData[indexPath.row]
        
        
        return cell
}

}
