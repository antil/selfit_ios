//
//  CustomButton.swift
//  Sonect
//
//  Created by Priyanka Antil on 20/08/18.
//  Copyright © 2018 valuecoders. All rights reserved.
//

import UIKit

class CustomButton :UIButton {
    
    override func awakeFromNib() {
        self.layoutIfNeeded()
        self.applyBorder(color:UIColor.white)
    }
    
    
    func applyBorder(color:UIColor) {
        self.layer.borderColor = color.cgColor
        self.layer.borderWidth = 1.0
        self.layer.cornerRadius = 5.0
        self.layer.masksToBounds = true
      
    }
}
