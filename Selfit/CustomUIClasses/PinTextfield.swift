//
//  PinTextfield.swift
//  Selfit
//
//  Created by Priyanka Antil on 24/08/18.
//  Copyright © 2018 valuecoders. All rights reserved.
//

import UIKit

class PinTextfield: UITextField, UITextFieldDelegate {

    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        
        textFieldBorder(textfield: self)
       // self.delegate = self
    }
    
    func textFieldBorder(textfield: UITextField)
    {
        textfield.layer .masksToBounds = true
        textfield.layer.borderColor = UIColor.white.cgColor
        textfield.layer.borderWidth = 0.8
        textfield.backgroundColor = UIColor.clear
        textfield.textColor = UIColor.white
    }
    
    func isEmpty() -> Bool {
        if let text = self.text {
            return text.count <= 0
        }
        return true
    }
    
    func addDoneButtonOnKeyboard() {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x:0, y:0, width: 320, height:50))
        doneToolbar.barTintColor = UIColor.init(red: 207/255.0, green: 210/255.0, blue: 219/255.0, alpha: 1.0)
        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: self, action: #selector(self.doneButtonAction))
        done.tintColor = UIColor.white
        let items = NSMutableArray()
        items.add(flexSpace)
        items.add(done)
        doneToolbar.items = items as? [UIBarButtonItem]
        doneToolbar.sizeToFit()
        self.inputAccessoryView = doneToolbar
    }
    
    @objc func doneButtonAction() {
        self.resignFirstResponder()
    }
    

}

