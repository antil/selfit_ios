//
//  CustomTextField.swift
//  Selfit
//
//  Created by Priyanka Antil on 20/08/18.
//  Copyright © 2018 valuecoders. All rights reserved.
//

import UIKit

class CustomTextField: UITextField, UITextFieldDelegate{
    
    let padding = UIEdgeInsets(top: 0, left: 14, bottom: 0, right: 5)
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        
        textFieldBorder(textfield: self)
        self.delegate = self
    }
    
    override open func textRect(forBounds bounds: CGRect) -> CGRect {
        return UIEdgeInsetsInsetRect(bounds, padding)
    }
    
    override open func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        return UIEdgeInsetsInsetRect(bounds, padding)
    }
    
    override open func editingRect(forBounds bounds: CGRect) -> CGRect {
        return UIEdgeInsetsInsetRect(bounds, padding)
    }
    
    func textFieldBorder(textfield: UITextField)
    {
        textfield.layer .masksToBounds = true
        textfield.layer.borderColor = UIColor.white.cgColor
        textfield.layer.borderWidth = 1.0
        textfield.layer.cornerRadius = 5.0
        textfield.backgroundColor = UIColor.white
        textfield.textColor = UIColor.darkGray
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.layer .masksToBounds = true
        textField.layer.borderColor = UIColor.init(red: 224.0/255.0, green: 0/255.0, blue: 32.0/255.0, alpha: 1.0).cgColor
        textField.layer.borderWidth = 1.0
        textField.layer.cornerRadius = 5.0
        textField.backgroundColor = UIColor.clear
        textField.textColor = UIColor.white
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.layer .masksToBounds = true
        textField.layer.borderColor = UIColor.init(red: 190.0/255.0, green: 207.0/255.0, blue: 235.0/255.0, alpha: 1.0).cgColor
        textField.layer.borderWidth = 1.0
        textField.layer.cornerRadius = 5.0
        textField.backgroundColor = UIColor.clear
        textField.textColor = UIColor.white
        if self.isEmpty() == true{
            self.textFieldBorder(textfield: self)
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.layer .masksToBounds = true
         textField.layer.borderColor = UIColor.init(red: 190.0/255.0, green: 207.0/255.0, blue: 235.0/255.0, alpha: 1.0).cgColor
        textField.layer.borderWidth = 1.0
        textField.layer.cornerRadius = 5.0
        textField.backgroundColor = UIColor.clear
        textField.textColor = UIColor.white
        textField.resignFirstResponder()
        return true
        
    }
    
    func isEmpty() -> Bool {
        if let text = self.text {
            return text.count <= 0
        }
        return true
    }
    
    
    func addImageToRight(txtfield: UITextField){
        txtfield.layer .masksToBounds = true
        txtfield.layer.borderColor = UIColor.init(red: 121.0/255.0, green: 157/255.0, blue: 213/255.0, alpha: 1.0).cgColor
        txtfield.layer.borderWidth = 1.0
        txtfield.layer.cornerRadius = 5.0
        txtfield.backgroundColor = UIColor.clear
        txtfield.textColor = UIColor.init(red: 121.0/255.0, green: 157/255.0, blue: 213/255.0, alpha: 1.0)
        
        let imgV = UIImageView(frame: CGRect(x: 0, y: 0, width: 17, height: 17))
        let image = UIImage(named: "dropdown")
        imgV.image = image
        txtfield.rightViewMode = UITextFieldViewMode.always
        txtfield.rightView = imgV
    }
    
    
    
    //MARK: - EmailValidations
    func isValidEmail(testStr:String) -> Bool
    {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
   
   
}

