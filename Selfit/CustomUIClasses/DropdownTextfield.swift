//
//  DropdownTextfield.swift
//  Selfit
//
//  Created by Priyanka Antil on 20/08/18.
//  Copyright © 2018 valuecoders. All rights reserved.
//

import UIKit
import DropDown

class DropdownTextfield: UITextField{
    
    let padding = UIEdgeInsets(top: 0, left: 14, bottom: 0, right: 5)
    let dropDown = DropDown()
 
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        
        textFieldBorder()
        let arrData = ["Delhi", "Chandigarh", "Mumbai", "Banglore"]
        addDropDown(dataArray: arrData)
      
        //self.delegate = self
    }
    
    override open func textRect(forBounds bounds: CGRect) -> CGRect {
        return UIEdgeInsetsInsetRect(bounds, padding)
    }
    
    override open func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        return UIEdgeInsetsInsetRect(bounds, padding)
    }
    
    override open func editingRect(forBounds bounds: CGRect) -> CGRect {
        return UIEdgeInsetsInsetRect(bounds, padding)
    }
    
    func textFieldBorder()
    {
        self.layer .masksToBounds = true
        self.layer.borderColor = UIColor.init(red: 121.0/255.0, green: 157/255.0, blue: 213/255.0, alpha: 1.0).cgColor
        self.layer.borderWidth = 1.0
        self.layer.cornerRadius = 5.0
        self.backgroundColor = UIColor.clear
        self.textColor = UIColor.init(red: 121.0/255.0, green: 157/255.0, blue: 213/255.0, alpha: 1.0)
        
        let imgV = UIImageView(frame: CGRect(x: 0, y: 0, width: 17, height: 17))
        let image = UIImage(named: "dropdown")
        imgV.image = image
        self.rightViewMode = UITextFieldViewMode.always
        self.rightView = imgV
    }
    
    func addDropDown(dataArray: Array<Any>){
        dropDown.dataSource = dataArray as! [String]
        
         dropDown.width = self.frame.width
         dropDown.textFont = UIFont.init(name: "Helvetica", size: 12.0)!
         dropDown.cellHeight = 30.0
         dropDown.backgroundColor = UIColor.init(red: 10/255.0, green: 30/255.0, blue: 130/255.0, alpha: 0.8)
         dropDown.textColor = UIColor.white
         dropDown.anchorView = self
         dropDown.topOffset = CGPoint(x: 15, y: (dropDown.anchorView?.plainView.bounds.height)!)
        
        self.dropDown.selectionAction = { [unowned self] (index, item) in
            self.text = self.dropDown.selectedItem
        }
      
    }
   
    
    func showDropDown(){
       dropDown.show()
    }
   
    func hideDropDown(){
       dropDown.hide()
    }

    func isEmpty() -> Bool {
        if let text = self.text {
            return text.count <= 0
        }
        return true
    }
}

