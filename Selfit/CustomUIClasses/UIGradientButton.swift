//
//  UIGradientButton.swift
//  Bailey
//
//  Created by vinove on 19/07/17.
//  Copyright © 2017 vinove. All rights reserved.
//

import UIKit
@IBDesignable

 class UIGradientButton: UIButton {

    @IBInspectable var topcolor: UIColor = UIColor.red
    @IBInspectable var bottomcolor: UIColor = UIColor.red
  
    
    override func draw(_ rect: CGRect) {
        layoutGradientButtonLayer()
    }
    
    override public func layoutSubviews() {
        super.layoutSubviews()
        
    }
    
    // MARK: Private
     func layoutGradientButtonLayer() {
        let gradient:CAGradientLayer = CAGradientLayer()
               // let topcolor = UIColor(red: 243.0/255.0, green: 49.0/255.0, blue: 93.0/255.0, alpha: 1.0).cgColor
                //let bottomcolor = UIColor(red: 250.0/255.0, green: 87.0/255.0, blue: 51.0/255.0, alpha: 1.0).cgColor
        
                gradient.colors = [self.topcolor.cgColor , self.bottomcolor.cgColor]
                gradient.startPoint = CGPoint(x: 0.0, y: 0.5)
                    // gradient.locations = [0.0, 1.0]
                gradient.endPoint = CGPoint(x: 1.0, y: 0.5)
                gradient.frame = CGRect(x: 0, y: 0, width: self.frame.size.width, height: self.frame.size.height)
                gradient.cornerRadius = 5
              self.layer.insertSublayer(gradient, at: 0)
        
                
    }
    

}
