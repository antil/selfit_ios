//
//  CongratsViewController.swift
//  Selfit
//
//  Created by vinove on 9/5/18.
//  Copyright © 2018 valuecoders. All rights reserved.
//

import UIKit

class CongratsViewController: SelfitViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        setUpNavigationBar(title: localized(key: .MyTraining))
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

}
