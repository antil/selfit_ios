//
//  MyTrainingViewController.swift
//  Selfit
//
//  Created by vinove on 9/5/18.
//  Copyright © 2018 valuecoders. All rights reserved.
//

import UIKit
import ExpandableCell

class MyTrainingViewController: SelfitViewController, ExpandableDelegate{

    @IBOutlet weak var instructorName: UILabel!
    @IBOutlet weak var tableView: ExpandableTableView!
    var arrFichasNames = [String]()
    var arrActivitiesNames = [String]()
  
    //MARK: - ViewLifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()

        //self.heightTableView.constant = CGFloat(44 * (arrData.count + 1) + 50)
        setUpNavigationBar(title: localized(key: .MyTraining))
        
         getTrainingData()
        
        tableView.expandableDelegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
       
        self.navigationController?.isNavigationBarHidden = false
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //MARK: - IBActions
    
    @IBAction func actionCompleted(_ sender: Any) {
        
        if let completedVC = self.storyboard?.instantiateViewController(withIdentifier: "CongratsViewController") as? CongratsViewController{
            self.navigationController?.pushViewController(completedVC, animated: true)
        }
    }
    

    //MARK: - WebServices
    func getTrainingData()
    {
        let queryItems = [NSURLQueryItem(name: "username", value: "46")]
        let urlComps = NSURLComponents(string: URLManager.manager.consultTrainingArea)
        urlComps?.queryItems = queryItems as [URLQueryItem]
        let parameterString = urlComps?.query
        
        if VTAlmofireHelper.sharedInstance.checkNetworkStatus() == true
        {
            ProgressHud.progressBarDisplayer(indicator: true, view: self.view)
            SelfitRequestViewController().consultTrainingArea(params: parameterString!) { (status, response) -> (Void) in
                
              ProgressHud.progressBarDisplayer(indicator: false, view: self.view)
                print(response)
                
                if let professorName:String = response["professorMontou"] as? String{
                    self.instructorName.text = professorName
                }else { self.instructorName.text = "" }
                
                if let arrFiles:Array = response["fichas"] as? Array<Any>{
                    if arrFiles.count>0{
                        for i in 0..<arrFiles.count{
                            let dict:NSDictionary = arrFiles[i] as! NSDictionary
                            guard let name:String = dict["nome"] as? String
                                else{
                                    return}
                            print(name)
                            self.arrFichasNames.append(name)
                        }
                    }
                }
                if let arrActivities:Array = response["atividades"] as? Array<Any>
                {
                    if arrActivities.count>0{
                        for i in 0..<arrActivities.count{
                            let dict:NSDictionary = arrActivities[i] as! NSDictionary
                            guard let name:String = dict["nome"] as? String
                                else{
                                    return}
                            print(name)
                            self.arrActivitiesNames.append(name)
                        }
                        
                    }
                    
                }
                
                
                 self.tableView.reloadData()
           }
        }
        else
        {
            _ = CustomAlertView().showAlert("", subTitle: localized(key: .NoInternet))
        }
        
    }
  

}


//MARK: - TableViewDelegates and Datasource
extension MyTrainingViewController{
    
    func expandableTableView(_ expandableTableView: ExpandableTableView, expandedCellsForRowAt indexPath: IndexPath) -> [UITableViewCell]? {
        
        let cell = self.tableView.dequeueReusableCell(withIdentifier: "expandedCell")
            as! ExpandableCell
        
        cell.backgroundColor = UIColor.clear
        cell.textLabel?.textColor = UIColor.white
        cell.textLabel?.font = UIFont(name: "Ubuntu", size: 15)
        if indexPath.row == 0{
            if arrFichasNames.count > 0{
                cell.textLabel?.text = self.arrFichasNames[indexPath.row]
            }
        }
        else{
            if arrActivitiesNames.count > 0{
                cell.textLabel?.text = self.arrActivitiesNames[indexPath.row]
            }
        }
        return [cell]
    }
    
    func expandableTableView(_ expandableTableView: ExpandableTableView, heightsForExpandedRowAt indexPath: IndexPath) -> [CGFloat]? {
        return [44]
    }

    func numberOfSections(in tableView: ExpandableTableView) -> Int {
        return 1
    }
    
    func expandableTableView(_ expandableTableView: ExpandableTableView, numberOfRowsInSection section: Int) -> Int {

     return 2
//        if arrFichasNames.count > 0{
//            return arrFichasNames.count
//        }
//        if arrActivitiesNames.count > 0{
//            return arrFichasNames.count
//        }
        
    }
    
    func expandableTableView(_ expandableTableView: ExpandableTableView, didSelectRowAt indexPath: IndexPath) {
        
        //        print("didSelectRow:\(indexPath)")
    }
    
    func expandableTableView(_ expandableTableView: ExpandableTableView, didSelectExpandedRowAt indexPath: IndexPath) {
        //        go to next
    }
    
    func expandableTableView(_ expandableTableView: ExpandableTableView, expandedCell: UITableViewCell, didSelectExpandedRowAt indexPath: IndexPath) {
//        if let cell = expandedCell as? ExpandedCell {
//            print("\(cell.titleLabel.text ?? "")")
//        }
    }
    
    func expandableTableView(_ expandableTableView: ExpandableTableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeueReusableCell(withIdentifier: "MyTrainingTableViewCell")
            as! ExpandableCell
        
        cell.backgroundColor = UIColor.clear
        cell.textLabel?.textColor = UIColor.white
         cell.textLabel?.font = UIFont(name: "Ubuntu-Bold", size: 15)
        if indexPath.row == 0{
                cell.textLabel?.text = localized(key: .TrainingSheet)
        }
        else{
                cell.textLabel?.text = localized(key: .Activities)
            
        }
        return cell

    }
    
    func expandableTableView(_ expandableTableView: ExpandableTableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44
    }
    
    func expandableTableView(_ expandableTableView: UITableView, didHighlightRowAt indexPath: IndexPath) {
        let cell = expandableTableView.cellForRow(at: indexPath)
        cell?.backgroundColor = UIColor.clear
    }
    
    func expandableTableView(_ expandableTableView: UITableView, shouldHighlightRowAt indexPath: IndexPath) -> Bool {
        return true
}
}
