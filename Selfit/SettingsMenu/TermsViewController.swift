//
//  TermsViewControllerr.swift
//  Selfit
//
//  Created by Priyanka Antil on 07/09/18.
//  Copyright © 2018 valuecoders. All rights reserved.
//

import UIKit

class TermsViewController: SelfitViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
         setUpNavigationBar(title: localized(key: .TermsCondition))
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    

}
