//
//  SettingOptionsViewController.swift
//  Selfit
//
//  Created by Priyanka Antil on 11/09/18.
//  Copyright © 2018 valuecoders. All rights reserved.
//

import UIKit
import CoreLocation
import LocalAuthentication

class SettingOptionsViewController: SelfitViewController, CLLocationManagerDelegate {
    
    @IBOutlet weak var imageV: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var activateBtn: UIButton!
    var rawValue = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()

       
        }
        
        override func viewWillAppear(_ animated: Bool) {
           
            self.navigationController?.isNavigationBarHidden = false
             self.setUI(option: SettingsControllers(rawValue: rawValue)!)
        }
        override func didReceiveMemoryWarning() {
            super.didReceiveMemoryWarning()
            // Dispose of any resources that can be recreated.
        }
        
       
    
   func setUI(option: SettingsControllers){
        if option == .ActivateLocation{
             setUpNavigationBar(title: localized(key: .ActivateLocation))
            lblTitle.text = "Access My Location"
            imageV.image = UIImage.init(named: "activate_location")
            activateBtn.setTitle("Access My Location", for: .normal)
            
        }else if option == .ActivateTouchID{
             setUpNavigationBar(title: localized(key: .ActivateTouchID))
            lblTitle.text = "Activate TouchId"
            imageV.image = UIImage.init(named: "activate_touchId")
            activateBtn.setTitle("Activate TouchId", for: .normal)
        }else {
             setUpNavigationBar(title: localized(key: .ActivateNotification))
            lblTitle.text = "Activate Notifications"
            imageV.image = UIImage.init(named: "activate_notifications")
            activateBtn.setTitle("Activate Notifications", for: .normal)
        }
        
    }
    
    @IBAction func notAcceptAction(_ sender: Any) {
    }
    
    @IBAction func acceptAction(_ sender: Any) {
        switch rawValue {
        case 1:
           showTouchIdPopUp()
            break
        case 2:
            if CLLocationManager.locationServicesEnabled()
            {
                switch CLLocationManager.authorizationStatus()
                {
                case .notDetermined, .restricted, .denied:
                    showLocationPopUp()
                    print("Denied")
                case .authorizedAlways, .authorizedWhenInUse:
                    print("Access")
                }
            }
            else
            {
                print("Location services are not enabled")
            }
            break
        case 3:
           showNotificationPopUp()
            break
        default:
            break
        }
        
    }
    
    @IBAction func activateAction(_ sender: Any) {
    }
    
    func showLocationPopUp()
    {
        let alert = CustomAlertView()
        alert.addButton(localized(key: .No), backgroundColor: UIColor.lightGray ) {
            alert.hideView()
        }
        alert.addButton(localized(key: .Yes), backgroundColor: UIColor.red ) {
           self.enableLocation()
        }
        alert.appearance.showCloseButton = false
        alert.showAlert("Location", subTitle: "Activate location?")
        
    }
    
    func enableLocation()
    {
        if let url = URL(string: "App-Prefs:root=Privacy&path=LOCATION/com.selfit")
        {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
    }
    
    func showTouchIdPopUp()
    {
        let alert = CustomAlertView()
        alert.addButton(localized(key: .No), backgroundColor: UIColor.lightGray ) {
            alert.hideView()
        }
        alert.addButton(localized(key: .Yes), backgroundColor: UIColor.red ) {
            self.authenticationWithTouchID()
        }
        alert.appearance.showCloseButton = false
        alert.showAlert("TouchId", subTitle: "Activate touchId?")
        
    }
    
    func showNotificationPopUp()
    {
        let alert = CustomAlertView()
        alert.addButton(localized(key: .No), backgroundColor: UIColor.lightGray ) {
            alert.hideView()
        }
        alert.addButton(localized(key: .Yes), backgroundColor: UIColor.red ) {
            self.enableLocation()
        }
        alert.appearance.showCloseButton = false
        alert.showAlert("Notification", subTitle: "Activate Notification?")
        
    }

    func authenticationWithTouchID() {
        let localAuthenticationContext = LAContext()
        localAuthenticationContext.localizedFallbackTitle = "Use Passcode"
        
        var authError: NSError?
        let reasonString = "To access the secure data"
        
        if localAuthenticationContext.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: &authError) {
            
            localAuthenticationContext.evaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, localizedReason: reasonString) { success, evaluateError in
                
                if success {
                    
                    //save touchId
                    
                } else {
                    //error
                    guard let error = evaluateError else {
                        return
                    }
                    
                    print(self.evaluateAuthenticationPolicyMessageForLA(errorCode: error._code))
                    
                }
            }
        } else {
            
            guard let error = authError else {
                return
            }
            //TODO: Show appropriate alert if biometry/TouchID/FaceID is lockout or not enrolled
            print(self.evaluateAuthenticationPolicyMessageForLA(errorCode: error.code))
        }
    }
    
    func evaluatePolicyFailErrorMessageForLA(errorCode: Int) -> String {
        var message = ""
        if #available(iOS 11.0, macOS 10.13, *) {
            switch errorCode {
            case LAError.biometryNotAvailable.rawValue:
                message = "Authentication could not start because the device does not support biometric authentication."
                
            case LAError.biometryLockout.rawValue:
                message = "Authentication could not continue because the user has been locked out of biometric authentication, due to failing authentication too many times."
                
            case LAError.biometryNotEnrolled.rawValue:
                message = "Authentication could not start because the user has not enrolled in biometric authentication."
                
            default:
                message = "Did not find error code on LAError object"
            }
        } else {
            switch errorCode {
            case LAError.touchIDLockout.rawValue:
                message = "Too many failed attempts."
                
            case LAError.touchIDNotAvailable.rawValue:
                message = "TouchID is not available on the device"
                
            case LAError.touchIDNotEnrolled.rawValue:
                message = "TouchID is not enrolled on the device"
                
            default:
                message = "Did not find error code on LAError object"
            }
        }
        
        return message;
    }
    
    func evaluateAuthenticationPolicyMessageForLA(errorCode: Int) -> String {
        
        var message = ""
        
        switch errorCode {
            
        case LAError.authenticationFailed.rawValue:
            message = "The user failed to provide valid credentials"
            
        case LAError.appCancel.rawValue:
            message = "Authentication was cancelled by application"
            
        case LAError.invalidContext.rawValue:
            message = "The context is invalid"
            
        case LAError.notInteractive.rawValue:
            message = "Not interactive"
            
        case LAError.passcodeNotSet.rawValue:
            message = "Passcode is not set on the device"
            
        case LAError.systemCancel.rawValue:
            message = "Authentication was cancelled by the system"
            
        case LAError.userCancel.rawValue:
            message = "The user did cancel"
            
        case LAError.userFallback.rawValue:
            message = "The user chose to use the fallback"
            
        default:
            message = evaluatePolicyFailErrorMessageForLA(errorCode: errorCode)
        }
        
        return message
    }
}

