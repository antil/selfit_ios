//
//  Constant.swift
//  Selfit
//
//  Created by Priyanka Antil on 20/08/18.
//  Copyright © 2018 valuecoders. All rights reserved.
//

import Foundation
import UIKit

 let MAX_HEIGHT = UIScreen.main.bounds.height
let MAX_WIDTH = UIScreen.main.bounds.width
let leftMenuCellTitles = ["HOME","MY PROFILE", "MY ACADEMY", "LESSONS AND EVENTS", "MY CHALLENGES", "SETTINGS", "LOG OUT"]
let leftMenuCellTitleImages = ["home","my_profile", "my_academy", "cal_small", "challenges", "settings", "log_out"]

//MARK: - Progress Indicator
class ProgressHud
{
    static let actInd: UIActivityIndicatorView = UIActivityIndicatorView()
    static let loadingView: UIView = UIView()
    
    func localized(key:TranslationKey) -> String {
        return NSLocalizedString(key.rawValue, comment: "")
    }
    
    static  func progressBarDisplayer(indicator:Bool,view : UIView )
    {
        if indicator == true {
            let x = (UIApplication.shared.keyWindow?.frame.size.width)!/2 - 40
            let y = (UIApplication.shared.keyWindow?.frame.size.height)!/2 - 40
            loadingView.frame = CGRect(x : x,y :  y,width :  80,height :  80)
            loadingView.backgroundColor = UIColor.init(red: 6/255.0, green: 35/255.0, blue: 128/255.0, alpha: 1.0)
            loadingView.clipsToBounds = true
            loadingView.layer.cornerRadius = 10
            actInd.frame = CGRect(x : 0.0,y: 0.0,width : 40.0,height : 40.0);
            actInd.activityIndicatorViewStyle =
                UIActivityIndicatorViewStyle.whiteLarge
            actInd.center = CGPoint(x :loadingView.frame.size.width / 2,
                                    y : loadingView.frame.size.height / 2);
         
            
            loadingView.addSubview(actInd)

            view.addSubview(loadingView)
            loadingView.isHidden = false
            actInd.startAnimating()
            view.isUserInteractionEnabled = false
        }
        else{
            view.isUserInteractionEnabled = true
            actInd.stopAnimating()
            loadingView.removeFromSuperview()
   
        }
        
    }

}


//MARK: - ChangingTextFieldDesign

struct Shadow
{
    static func viewShadow(view:UIView)
    {
        view.layer.masksToBounds = false
        view.layer.shadowColor = UIColor.black.cgColor
        view.layer.shadowOpacity = 0.3
        view.layer.shadowOffset = CGSize(width: 1, height: 1)
        view.layer.shadowRadius = 2.0
        view.layer.shadowPath = UIBezierPath(rect: view.bounds).cgPath
        view.layer.shouldRasterize = true
    }
    
    static func textfieldShadow(textfield: UITextField)
    {
        textfield.layer .masksToBounds = true
        textfield.layer.borderColor = UIColor.init(red: 241.0/255.0, green: 241.0/255.0, blue: 241.0/255.0, alpha: 1.0).cgColor
        textfield.layer.borderWidth = 1.5
    }
    
    static func textShadow(view: UIView)
    {
        view.layer.masksToBounds = false
        view.layer.shadowRadius = 3.0
        view.layer.shadowColor = UIColor.black.cgColor
        view.layer.shadowOffset = CGSize(width: 1.0, height: 1.0)
        view.layer.shadowOpacity = 0.5
    }
    
//    class ProgressHud
//    {
//        static let actInd: UIActivityIndicatorView = UIActivityIndicatorView()
//        static let loadingView: UIImageView = UIImageView()
//
//        static  func progressBarDisplayer(indicator:Bool,view : UIView )
//        {
//            if indicator == true {
//                let x = (UIApplication.shared.keyWindow?.frame.size.width)!/2 - 40
//                let y = (UIApplication.shared.keyWindow?.frame.size.height)!/2 - 40
//                loadingView.image = UIImage.init(named: "loader")
//                loadingView.frame = CGRect(x : x,y :  y,width :  80,height :  80)
//                loadingView.backgroundColor = UIColor.black
//                loadingView.alpha = 0.7
//                loadingView.clipsToBounds = true
//                loadingView.layer.cornerRadius = 10
//                actInd.frame = CGRect(x : 0.0,y: 0.0,width : 40.0,height : 40.0);
//                actInd.activityIndicatorViewStyle =
//                    UIActivityIndicatorViewStyle.whiteLarge
//                actInd.center = CGPoint(x :loadingView.frame.size.width / 2,
//                                        y : loadingView.frame.size.height / 2);
//                //actInd.color = UIColor.init(red: 29.0/255.0, green: 14.0/255.0, blue: 106.0/255.0, alpha: 1.0)
//                loadingView.addSubview(actInd)
//                view.addSubview(loadingView)
//                loadingView.isHidden = false
//                actInd.startAnimating()
//                view.isUserInteractionEnabled = false
//            }
//            else{
//                view.isUserInteractionEnabled = true
//                actInd.stopAnimating()
//                loadingView.removeFromSuperview()
//
//            }
//
//        }
//
//    }
//
   
}



struct TextfieldImage
{
    static func textfieldImageRight(txt : UITextField) -> UITextField
    {
        let imgV = UIImageView(frame: CGRect(x: 0, y: 0, width: 17, height: 17))
        let image = UIImage(named: "dropdown")
        imgV.image = image
        txt.rightViewMode = UITextFieldViewMode.always
        txt.rightView = imgV
        return txt
    }
    
}













