//
//  Enums.swift
//  Selfit
//
//  Created by on Priyanka Antil 07/09/18.
//  Copyright © 2018 valuecoders. All rights reserved.
//

import Foundation

enum SettingsControllers: Int {
    case ChangePassword = 0
    case ActivateTouchID
    case ActivateLocation
    case ActivateNotification
    case AboutSelfit
    case SendFeedBack
    case ShareApplication
    case RateOnAppstore
    case TermsAndCondition
}
public enum NavigationFlow:String {
    case None
    case AboutSelfitController = "AboutSelfitController"
    case SendFeedbackController = "SendFeedbackController"
    case TermsViewController = "TermsViewController"
    case SettingsOptionViewController = "SettingOptionsViewController"
}


