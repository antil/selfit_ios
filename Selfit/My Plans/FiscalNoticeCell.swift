//
//  FiscalNoticeCell.swift
//  Selfit
//
//  Created by Priyanka Antil on 14/09/18.
//  Copyright © 2018 valuecoders. All rights reserved.
//

import UIKit

class FiscalNoticeCell: UITableViewCell {

    
    @IBOutlet weak var lblNotes: UILabel!
    
    @IBOutlet weak var lblPrint: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
