//
//  CreditCardChangeViewController.swift
//  Selfit
//
//  Created by vinove on 9/6/18.
//  Copyright © 2018 valuecoders. All rights reserved.
//

import UIKit

class CreditCardChangeViewController: SelfitViewController {

    @IBOutlet weak var lblCardNumber: UILabel!
    @IBOutlet weak var cardFlag: UILabel!
    @IBOutlet weak var lblMonth: UILabel!
    @IBOutlet weak var lblYear: UILabel!
    
    @IBOutlet weak var txtCardNumber: UITextField!
    
    @IBOutlet weak var txtCardHolderCPF: UITextField!
    @IBOutlet weak var txtMaturity: UITextField!
    @IBOutlet weak var `switch`: UISwitch!
    @IBOutlet weak var btnChangeCard: UIButton!
    
    //MARK: - ViewLifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()

        self.btnChangeCard.layer.cornerRadius = 7.0
        self.btnChangeCard.layer.masksToBounds = true
        self.btnChangeCard.layer.borderColor = UIColor.white.cgColor
        self.btnChangeCard.layer.borderWidth = 0.5
        
        self.setTextField(textField: self.txtCardNumber)
        self.setTextField(textField: self.txtMaturity)
        self.setTextField(textField: self.txtCardHolderCPF)
        
        setUpNavigationBar(title: localized(key: .ChangeCreditCard))
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
        getDccPaymentInformation()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
   
    
    func setTextField(textField:UITextField){
        
        textField.layer.cornerRadius = 7.0
        textField.layer.masksToBounds = true
        textField.layer.borderColor = UIColor.white.cgColor
        textField.layer.borderWidth = 0.5
    
    }

    
    //MARK: - IBAction
    @IBAction func btnClickChangeCard(_ sender: UIButton) {
        
        
    }
    
    //MARK: - WebServices
    func getDccPaymentInformation()
    {
        let queryItems = [NSURLQueryItem(name: "cliente", value: "7675"), NSURLQueryItem(name: "operadoraCartao", value: "VISA"), NSURLQueryItem(name: "numeroCartao", value: "4556680626578388") ,NSURLQueryItem(name: "validadeCartao", value: "04/18"),NSURLQueryItem(name: "convenioCobranca", value: "1"),NSURLQueryItem(name: "cpfTitular", value: "030.241.370-76"),NSURLQueryItem(name: "titularCartao", value: "cliente pacto")]
        let urlComps = NSURLComponents(string: URLManager.manager.dccPayment)
        urlComps?.queryItems = queryItems as [URLQueryItem]
        let parameterString = urlComps?.query
        
        if VTAlmofireHelper.sharedInstance.checkNetworkStatus() == true
        {
           ProgressHud.progressBarDisplayer(indicator: true, view: self.view)
            SelfitRequestViewController().getDccPayment(params: parameterString!) { (status, response) -> (Void) in
          
            ProgressHud.progressBarDisplayer(indicator: false, view: self.view)
                guard let message:String = response["return"] as? String
                    else { return }
                print(message)
                _ = CustomAlertView().showAlert("", subTitle: message)
            }
        }
        else
        {
            _ = CustomAlertView().showAlert("", subTitle: localized(key: .NoInternet))
        }
        
    }
    
    func changeDccPayment()
    {
        let queryItems = [NSURLQueryItem(name: "cliente", value: "7675"), NSURLQueryItem(name: "operadoraCartao", value: "VISA"), NSURLQueryItem(name: "numeroCartao", value: "4556680626578388") ,NSURLQueryItem(name: "validadeCartao", value: "04/18"),NSURLQueryItem(name: "convenioCobranca", value: "1"),NSURLQueryItem(name: "cpfTitular", value: "030.241.370-76"),NSURLQueryItem(name: "titularCartao", value: "cliente pacto")]
        let urlComps = NSURLComponents(string: URLManager.manager.dccPayment)
        urlComps?.queryItems = queryItems as [URLQueryItem]
        let parameterString = urlComps?.query
        
        if VTAlmofireHelper.sharedInstance.checkNetworkStatus() == true
        {
            ProgressHud.progressBarDisplayer(indicator: true, view: self.view)
            SelfitRequestViewController().getDccPayment(params: parameterString!) { (status, response) -> (Void) in
                
             ProgressHud.progressBarDisplayer(indicator: false, view: self.view)
                guard let message:String = response["return"] as? String
                    else { return }
                print(message)
                _ = CustomAlertView().showAlert("", subTitle: message)
            }
        }
        else
        {
            _ = CustomAlertView().showAlert("", subTitle: localized(key: .NoInternet))
        }
        
    }
    
    

}
