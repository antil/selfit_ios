//
//  ChangeCreditOptionsViewController.swift
//  Selfit
//
//  Created by vinove on 9/6/18.
//  Copyright © 2018 valuecoders. All rights reserved.
//

import UIKit

class ChangeCreditOptionsViewController: SelfitViewController {

    @IBOutlet weak var btnPaymentHistory: UIButton!
    @IBOutlet weak var btnChangeCredit: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
   
        self.btnChangeCredit.layer.cornerRadius = 5.0
        self.btnChangeCredit.layer.masksToBounds = true
        self.btnChangeCredit.layer.borderColor = UIColor.white.cgColor
        self.btnChangeCredit.layer.borderWidth = 0.5
        
        self.btnPaymentHistory.layer.cornerRadius = 5.0
        self.btnPaymentHistory.layer.masksToBounds = true
        self.btnPaymentHistory.layer.borderColor = UIColor.white.cgColor
        self.btnPaymentHistory.layer.borderWidth = 0.5

        setUpNavigationBar(title: localized(key: .PaymentStatus))
       
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
   

    @IBAction func btnChangePayment(_ sender: Any) {
    }
    @IBAction func btnCgangeCredit(_ sender: Any) {
    }
    
}
