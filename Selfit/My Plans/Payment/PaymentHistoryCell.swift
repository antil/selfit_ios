//
//  PaymentHistoryCell.swift
//  Selfit
//
//  Created by Priyanka Antil on 12/09/18.
//  Copyright © 2018 valuecoders. All rights reserved.
//

import UIKit

class PaymentHistoryCell: UITableViewCell {

    @IBOutlet weak var lblPayment: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var lblMaturity: UILabel!
    @IBOutlet weak var lblMonthlyPayment: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
