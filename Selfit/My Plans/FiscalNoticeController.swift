//
//  FiscalNoticeController.swift
//  Selfit
//
//  Created by Priyanka Antil on 14/09/18.
//  Copyright © 2018 valuecoders. All rights reserved.
//

import UIKit

class FiscalNoticeController: SelfitViewController{
    
    @IBOutlet weak var viewTop: UIView!
    
     //MARK: - ViewLifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
       setUpNavigationBar(title: localized(key: .FiscalNote))
        viewTop.layer.masksToBounds = true
        viewTop.layer.borderWidth = 0.6
        viewTop.layer.cornerRadius = 8.0
        viewTop.layer.borderColor = UIColor.white.cgColor
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
   
   
}

extension FiscalNoticeController : UITableViewDataSource, UITableViewDelegate
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 6
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FiscalNoticeCell") as! FiscalNoticeCell
        
        return cell
    }
    
}
