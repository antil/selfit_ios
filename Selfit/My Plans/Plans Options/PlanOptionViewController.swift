//
//  PlanOptionViewController.swift
//  Selfit
//
//  Created by vinove on 9/5/18.
//  Copyright © 2018 valuecoders. All rights reserved.
//

import UIKit

class PlanOptionViewController: SelfitViewController, UITableViewDataSource, UITableViewDelegate {
    
    let arrPlans = ["LET YOU CHANGE YOUR CURRENT UNITY?", "DO YOU WANT TO CHANGE TO BLUE FLOOR?","FREQUENCY","PAYMENT STATUS","ISSUE 2ND FISCAL NOTE"]
    let arrImg = ["twocrossArrow","arrowUp","heartBeat","dollars","redFrame",]
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setUpNavigationBar(title: localized(key: .MyPlan))
    }
  
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
   
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrPlans.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PlansOptionTableViewCell") as! PlansOptionTableViewCell
        cell.lblPlaneName.text = self.arrPlans[indexPath.row]
        //cell.lblPlaneName.font = UIFont.systemFont(ofSize: 12.0)
        cell.imgLogo.image = UIImage(named: self.arrImg[indexPath.row])
        
        if indexPath.row == 2{
            cell.accessoryType = .none
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 3{
            
            if let creditVC = self.storyboard?.instantiateViewController(withIdentifier: "ChangeCreditOptionsViewController")
                as? ChangeCreditOptionsViewController{
                self.navigationController?.pushViewController(creditVC, animated: true)
            }
            
        }else if indexPath.row == 4{
            if let creditVC = self.storyboard?.instantiateViewController(withIdentifier: "FiscalNoticeController")
                as? FiscalNoticeController{
                self.navigationController?.pushViewController(creditVC, animated: true)
            }
            
        }
    }
    
    

}
