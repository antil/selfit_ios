//
//  PlansOptionTableViewCell.swift
//  Selfit
//
//  Created by vinove on 9/5/18.
//  Copyright © 2018 valuecoders. All rights reserved.
//

import UIKit

class PlansOptionTableViewCell: UITableViewCell {
    @IBOutlet weak var imgLogo: UIImageView!
    @IBOutlet weak var lblPlaneName: UILabel!
    @IBOutlet weak var btnDetails: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
