//
//  ProfileViewController.swift
//  Selfit
//
//  Created by vinove on 9/4/18.
//  Copyright © 2018 valuecoders. All rights reserved.
//

import UIKit

class ProfileViewController: SelfitViewController,UITextFieldDelegate,UIImagePickerControllerDelegate, UINavigationControllerDelegate{
   
    
    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    
    @IBOutlet weak var lblProfileName: UILabel!
    @IBOutlet weak var imgUser: UIImageView!
    var imagePicker = UIImagePickerController()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpNavigationBar(title: localized(key: .Profile))
        
//        self.imgUser.layer.cornerRadius = self.imgUser.frame.size.width/2.0
//        self.imgUser.layer.masksToBounds = true
//        self.imgUser.layer.borderColor = UIColor.white.cgColor
//        self.imgUser.layer.borderWidth = 2.0
        self.txtName.delegate = self
        self.txtEmail.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setNavigationBarItem()
    }
    
    @IBAction func btnAddPic(_ sender: Any) {
        self.upload()
    }
   
    
    //MARK: - UploadImageMethods
    func upload()
    {
//         let alert =  CustomAlertView()
//         alert.showAlert("Selfit wants to access your photos", subTitle: "To be able to change your profile photo, allow access to the camera.", closeButtonTitle: "DO NOT ALLOW")
//        alert.addButton("ALLOW") {
//            <#code#>
//        }
        
//        let alert =  CustomAlertView()
//        alert.showAlert("", subTitle: "", closeButtonTitle: "CANCEL")
//        alert.appearance.showCloseButton = false
//        alert.addButton("CAMERA") {
//            self.openCamera()
//        }
//        alert.addButton("GALLERY") {
//             self.openGallery()
//        }
        let alert:UIAlertController = UIAlertController(title: "Choose Image", message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
        let cameraAction = UIAlertAction(title: "Camera", style: UIAlertActionStyle.default)
        {
            UIAlertAction in
            self.openCamera()
        }
        let galleryAction = UIAlertAction(title: "Gallery", style: UIAlertActionStyle.default)
        {
            UIAlertAction in
            self.openGallery()
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel)
        {
            UIAlertAction in
        }

       
         imagePicker.delegate = self
        alert.addAction(cameraAction)
        alert.addAction(galleryAction)
        alert.addAction(cancelAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    func openGallery(){
        imagePicker.allowsEditing = false
        imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
        present(imagePicker, animated: true, completion: nil)
    }
    
    
    func openCamera(){
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.camera)){
            imagePicker.allowsEditing = false
            imagePicker.sourceType = UIImagePickerControllerSourceType.camera
            imagePicker.cameraCaptureMode = .photo
            present(imagePicker, animated: true, completion: nil)
        }
        else{
            let alert = UIAlertController(title: "Camera Not Found", message: "This device has no Camera", preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style:.default, handler: nil)
            alert.addAction(ok)
            present(alert, animated: true, completion: nil)
        }
    }
    
    //MARK: - ImagePickerDelegates
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info:  [String : Any]) {
        let chosenImage = info[UIImagePickerControllerOriginalImage] as! UIImage
        self.imgUser.image = chosenImage
        
        dismiss(animated: true, completion: nil)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btnProfileDetail(_ sender: Any) {
    }

}
