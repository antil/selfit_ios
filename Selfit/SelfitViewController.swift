//
//  SelfitViewController.swift
//  Selfit
//
//  Created by Priyanka Antil on 20/08/18.
//  Copyright © 2018 valuecoders. All rights reserved.
//

import UIKit

class SelfitViewController: UIViewController {

    let mainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    func setUpNavigationBar(title: String){
        let button = UIBarButtonItem(image: UIImage(named:"back")?.withRenderingMode(.alwaysOriginal), style: .plain, target: self, action: #selector(self.back))
        self.navigationItem.leftBarButtonItem = button
        self.navigationItem.title = title
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.font: UIFont(name: "Ubuntu-Bold", size: 18)! , NSAttributedStringKey.foregroundColor:UIColor.white]
        self.navigationController?.navigationBar.barTintColor = UIColor.init(red: 6/255.0, green: 35/255.0, blue: 128/255.0, alpha: 1.0)
    }
    
    @objc func back()
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    func localized(key:TranslationKey) -> String {
        return NSLocalizedString(key.rawValue, comment: "")
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }


        override var preferredStatusBarStyle: UIStatusBarStyle
        {
            return .lightContent
        }
}

