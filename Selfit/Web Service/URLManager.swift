//
//  URLManager.swift
//  Jewellery
//
//  Created by vinove on 7/19/18.
//  Copyright © 2018 vinove. All rights reserved.
//

import Foundation


class URLManager{
    
    static let manager = URLManager()
   
    private var baseUrl:String = "http://lb-zw-api-624377300.sa-east-1.elb.amazonaws.com/api/prest/"
    private var clienteUrl:String = "http://lb-zw-api-624377300.sa-east-1.elb.amazonaws.com/api/prest/cliente/8b682e93b8f5a3548c46a82e9214f9bc/"
    
   
    lazy var register:String = {
        var base = self.clienteUrl + "cadastrarCliente"
        return  base
    }()
    
    lazy var login:String = {
        var base = self.clienteUrl + "logar"
        return  base
    }()
    lazy var recoverPassword:String = {
        var base = self.clienteUrl + "recuperarUsuarioMovel"
        return  base
    }()
    lazy var changePassword:String = {
        var base = self.clienteUrl + "alterarSenhaUsuarioMovel"
        return  base
    }()
    lazy var getCities:String = {
        var base = self.clienteUrl + "consultarCidades"
        return  base
    }()
    lazy var getStates:String = {
        var base = self.clienteUrl + "consultarEstados"
        return  base
    }()
    lazy var consultNetworsCompanies:String = {
        var base = self.baseUrl + "redeempresa/8b682e93b8f5a3548c46a82e9214f9bc/consultarRedeEmpresaSite"
        return  base
    }()
    lazy var gymPlans:String = {
        var base = self.baseUrl + "negociacao/8b682e93b8f5a3548c46a82e9214f9bc/consultarPlanos"
        return  base
    }()
    lazy var dccPayment:String = {
        var base = self.baseUrl + "negociacao/8b682e93b8f5a3548c46a82e9214f9bc/incluirAutorizacaoCobranca"
        return  base
    }()
    
    lazy var changeDccPayment:String = {
        var base = self.baseUrl + "negociacao/8b682e93b8f5a3548c46a82e9214f9bc/alterarAutorizacaoCobranca"
        return  base
    }()
    
    lazy var consultTrainingArea:String = {
        var base = "http://lb-tr-web-1426930111.sa-east-1.elb.amazonaws.com/TreinoWeb/prest/programa/8b682e93b8f5a3548c46a82e9214f9bc/atual"
        return  base
    }()
    
}
