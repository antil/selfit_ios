//
//  SelfitRequestViewController.swift
//  Selfit
//
//  Created by Priyanka Antil on 04/09/18.
//  Copyright © 2018 valuecoders. All rights reserved.
//

import UIKit
import Foundation
import Alamofire
import Reachability

class SelfitRequestViewController {

    
    func register(params:String,compilition:@escaping (_ status:Bool,_ response:NSDictionary) ->(Void)){
        
        VTAlmofireHelper.sharedInstance.webHelper(method:"POST", postString:params, url:URLManager.manager.register){
            (response:Dictionary,success:Bool) in
            print(response)
            
            if(success)
            {
                compilition(true,response as NSDictionary)
                 print("user registered")

            }else{
                compilition(false,[:] as NSDictionary)
            }
            
        }
        
    }
    
    
    func login(params:String,compilition:@escaping (_ status:Bool,_ response:[UserData]) ->(Void)){
        
        VTAlmofireHelper.sharedInstance.webHelper(method:"POST", postString:params, url:URLManager.manager.login){
            (response:Dictionary,success:Bool) in
            print(response)
            
            if(success)
            {
                if let dictResponse:Dictionary = response["return"] as? [String:Any]{
                    
                    ParseUserData().parseData(params: dictResponse, compilition: { (status, result) -> (Void) in
                        compilition(true,[result])
                    })
                  
                }}
                else{
                    guard let message:String = response ["erro"] as? String
                    else
                    { return }
                 _ = CustomAlertView().showAlert("", subTitle: message)
                    compilition(false,[UserData]())
          
            }
        }
    }
    
    func getCities(params:String, compilition:@escaping (_ status:Bool,_ response:[Cities]) ->(Void)){
        
        VTAlmofireHelper.sharedInstance.webHelper(method:"POST", postString:params, url:URLManager.manager.getCities){
            (response:Dictionary,success:Bool) in
            if(success){
                if let dictResponse:Array<Dictionary> = response["return"] as? Array<[String:Any]>{
                    ParseCities().parseData(params: dictResponse){(status, result) -> (Void) in
                        compilition(true,result)
                    }
                }
            }else{
                compilition(false,[Cities]())
            }
            
        }
        
    }
    
    func getStates(compilition:@escaping (_ status:Bool,_ response:[States]) ->(Void)){
        
        VTAlmofireHelper.sharedInstance.webHelper(method:"POST", postString:"", url:URLManager.manager.getStates){
            (response:Dictionary,success:Bool) in
            if(success){
                if let dictResponse:Array<Dictionary> = response["return"] as? Array<[String:Any]>{
                    ParseStates().parseData(params: dictResponse){(status, result) -> (Void) in
                        compilition(true,result)
                    }
                }
            }else{
                compilition(false,[States]())
            }
            
        }
        
    }
    
    func getGymPlans(params:String, compilition:@escaping (_ status:Bool,_ response:[GymPlans]) ->(Void)){
        
        VTAlmofireHelper.sharedInstance.webHelper(method:"POST", postString:params, url:URLManager.manager.gymPlans){
            (response:Dictionary,success:Bool) in
            if(success){
                if let dictResponse:Array<Dictionary> = response["return"] as? Array<[String:Any]>{
                    ParseGymPlans().parseData(params: dictResponse){(status, result) -> (Void) in
                        compilition(true,result)
                    }
                }
            }else{
                compilition(false,[GymPlans]())
            }
            
        }
        
    }
    func recoverPassword(params:String, compilition:@escaping (_ status:Bool,_ response:NSDictionary) ->(Void)){
        
        VTAlmofireHelper.sharedInstance.webHelper(method:"POST", postString:params, url:URLManager.manager.recoverPassword){
            (response:Dictionary,success:Bool) in
            print(response)
            
            if(success)
            {
                compilition(true,response as NSDictionary)
                
            }else{
                
                compilition(false,[:] as NSDictionary)
            }
            
        }
        
    }
    
    
    func changePassword(params:String, compilition:@escaping (_ status:Bool,_ response:NSDictionary) ->(Void)){
        
        VTAlmofireHelper.sharedInstance.webHelper(method:"POST", postString:params, url:URLManager.manager.changePassword){
            (response:Dictionary,success:Bool) in
            print(response)
            
            if(success)
            {
                compilition(true,response as NSDictionary)
                
            }else{
                
                compilition(false,[:] as NSDictionary)
            }
            
        }
        
    }
        
    func consultNetworkCompanies(params:String, compilition:@escaping (_ status:Bool,_ response:[NetworkCompanies]) ->(Void)){
       
        VTAlmofireHelper.sharedInstance.webHelper(method:"POST", postString:params, url:URLManager.manager.consultNetworsCompanies){
            (response:Dictionary,success:Bool) in
            if(success){
                if let dictResponse:Array<Dictionary> = response["return"] as? Array<[String:Any]>{
                    ParseNetworkCompanies().parseData(params: dictResponse){(status, result) -> (Void) in
                        compilition(true,result)
                    }
                }
            }else{
                compilition(false,[NetworkCompanies]())
            }
            
        }
        
    }
    
    func getDccPayment(params:String, compilition:@escaping (_ status:Bool,_ response:NSDictionary) ->(Void)){
        
        VTAlmofireHelper.sharedInstance.webHelper(method:"POST", postString:params, url:URLManager.manager.dccPayment){
            (response:Dictionary,success:Bool) in
            print(response)
            
            if(success)
            {
                compilition(true,response as NSDictionary)
                
            }else{
                
                compilition(false,[:] as NSDictionary)
            }
        }
    }
    
    func changeDccPayment(params:String, compilition:@escaping (_ status:Bool,_ response:NSDictionary) ->(Void)){
        
        VTAlmofireHelper.sharedInstance.webHelper(method:"POST", postString:params, url:URLManager.manager.changeDccPayment){
            (response:Dictionary,success:Bool) in
            print(response)
            
            if(success)
            {
                compilition(true,response as NSDictionary)
                
            }else{
                
                compilition(false,[:] as NSDictionary)
            }
        }
    }
    
    func consultTrainingArea(params:String, compilition:@escaping (_ status:Bool,_ response:NSDictionary) ->(Void)){
        
        VTAlmofireHelper.sharedInstance.webHelper(method:"POST", postString:params, url:URLManager.manager.consultTrainingArea){
            (response:Dictionary,success:Bool) in
            print(response)

            if(success)
            {
                let dictPrograma:NSDictionary = (response["programa"] as? NSDictionary)!
               
//                ParseTrainingArea().parseData(params: dictResponse){(status, result) -> (Void) in
                   compilition(true,dictPrograma)
//                }
             //  compilition(true,response as NSDictionary)
                
            }else{
                
                compilition(false,[:])
            }
        }
    }
    
}

