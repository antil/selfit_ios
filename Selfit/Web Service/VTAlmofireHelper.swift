//
//  VTAlmofireHelper.swift
//
//
//  Created by Varun Tyagi on 15/12/16.
//  Copyright © 2016 Vinove. All rights reserved.
//

import UIKit
import Alamofire
import Foundation
import Reachability

typealias CompletionHandler = (_ response:Dictionary<String, Any>,_ success:Bool) -> Void

class VTAlmofireHelper: NSObject
{
    var responseString:NSString = ""
    
    class var sharedInstance:VTAlmofireHelper
    {
        struct Singleton {
            static let instance = VTAlmofireHelper()
        }
        return Singleton.instance
    }
    
    
    func checkNetworkStatus() ->  Bool
    {
        var isConnected:Bool!
        let reachability = Reachability()!
        
        if reachability.connection == .wifi || reachability.connection == .cellular
        {
            print("connection available")
            isConnected = true
        }
        else if reachability.connection == .none
        {
            print("connection lost")
            isConnected = false
        }
        else
        {
            print("No network Connecion")
            isConnected = false
        }
        return isConnected
    }
    
    func webHelper( method:String, postString:String ,url:String, completionHandler: @escaping CompletionHandler) {
        var request = URLRequest(url:URL(string:url)!)
 
        request.httpMethod = method
        request.httpBody = postString.data(using: String.Encoding.utf8)

        request.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        request.timeoutInterval = 300
        print(request)
        Alamofire.request(request)
            .responseJSON{ response in
                switch response.result {
                    
                case .failure(let error):
                    
                    print(error)
                    let  dict:[String:AnyObject] = Dictionary()
                    completionHandler(dict ,false)
                    
                case .success(let responseObject):
                    print(responseObject)
                    completionHandler(responseObject as! Dictionary ,true)
                }
        }
    }
    
    
    func webHelperWithToken( method : String,postString:String , url: String, authentication:String, completionHandler: @escaping CompletionHandler) {
        
        
        var request = URLRequest(url:URL(string:url)!)
        request.httpMethod = method
        request.httpBody = postString.data(using: String.Encoding.utf8)
        
       //request.addValue("application/json", forHTTPHeaderField: "Accept")
       // request.addValue("en-US", forHTTPHeaderField: "Accept-Language")
        request.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        if postString != ""{
          //request.addValue(authentication, forHTTPHeaderField: "Authorization")
        }
         request.addValue(authentication, forHTTPHeaderField: "Authorization")
        request.timeoutInterval = 300
        print(request)
        Alamofire.request(request)
            .responseJSON{ response in
                switch response.result {
                    
                case .failure(let error):
                    
                    print(error)
                    let  dict:[String:AnyObject] = Dictionary()
                    completionHandler(dict ,false)
                    
                case .success(let responseObject):
                    print(responseObject)
                    completionHandler(responseObject as! Dictionary ,true)
                }
        }
    }
    
   
    
    func webHelperToken( method : String,jsonRequest:NSMutableDictionary , url: String ,completionHandler: @escaping CompletionHandler) {
        
        var request = URLRequest(url:URL(string:url)!)
        request.httpMethod = method
        
        let data = try! JSONSerialization.data(withJSONObject: jsonRequest, options: [])
        request.httpBody = data
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        //request.addValue("X-HTTP-Method-Override", forHTTPHeaderField: "Accept-Encoding")
        request.addValue("en-US", forHTTPHeaderField: "Accept-Language")
        //request.addValue("323232ewdsd", forHTTPHeaderField: "appKey")
        request.addValue("chunked", forHTTPHeaderField: "transfer-encoding")
        request.timeoutInterval = 300
        Alamofire.request(request)
            .responseJSON{ response in
                switch response.result {
                    
                case .failure(let error):
                    
                    print(error)
                    let  dict:[String:AnyObject] = Dictionary()
                    completionHandler(dict ,false)
                    
                case .success(let responseObject):
                    print(responseObject)
                    completionHandler(responseObject as! Dictionary ,true)
                    
                }
        }
        
        
    }
    
    func post(with url:URL,method : String,params:Dictionary<String,Any>?,complition:@escaping (_ responce:Dictionary<String,Any>?,_ error:Error?)->(Void)){

        let request = NSMutableURLRequest(url: url)
        request.httpMethod = "POST"
        // request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        request.addValue("X-HTTP-Method-Override", forHTTPHeaderField: "Accept-Encoding")
        request.addValue("en-US", forHTTPHeaderField: "Accept-Language")
        request.addValue("323232ewdsd", forHTTPHeaderField: "appKey")
        request.addValue("chunked", forHTTPHeaderField: "transfer-encoding")
        request.timeoutInterval = 300
        let postData = NSMutableData()
        var i = 0
        for (key,value) in params!{
            if (i == 0){
                postData.append("\(key)=\(value)".data(using: String.Encoding.utf8)!)
            }else{
                postData.append("&\(key)=\(value)".data(using: String.Encoding.utf8)!)
            }
            i = i + 1
        }
        request.httpBody = postData as Data
        let session = URLSession.shared
        let task = session.dataTask(with: request as URLRequest) { (data, responce, error) in
            if error == nil {
                let responseString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
                print("responseString = \(responseString!)")
                do {
                    let json = try JSONSerialization.jsonObject(with: data!, options: []) as? [String: Any]
                    // let response = json?["response"] as! Dictionary<String,Any>
                    complition(json,nil)

                }catch{
                    complition(nil,nil)
                }
            }else{
                complition(nil,error)
                print("Error --------------: \(String(describing: error?.localizedDescription))")
            }
        }
        task.resume()
    }

    
    
}



