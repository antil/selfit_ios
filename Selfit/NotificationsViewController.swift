//
//  NotificationsViewController.swift
//  Selfit
//
//  Created by Priyanka Antil on 11/09/18.
//  Copyright © 2018 valuecoders. All rights reserved.
//

import UIKit

class NotificationsViewController: SelfitViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

         setUpNavigationBar(title: localized(key: .Notifications))
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

extension NotificationsViewController: UITableViewDelegate, UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return 6
}

func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell:NotificationTableViewCell = tableView.dequeueReusableCell(withIdentifier: "notificationCell") as! NotificationTableViewCell
    cell.lblTitle.text = "MANUTENÇÃO"
    cell.lblDescription.text = "Caro utilizador, informamos que teremos o serviço temporariamente"
    cell.lblTime.text = "15:26"
    return cell
}
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let vc = self.storyboard?.instantiateViewController(withIdentifier: "NotificationsDetailController") as? NotificationsDetailController
        {
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
}
